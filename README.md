# DevOps System Provisioning

A collection of provisioning templates and scripts to provide a complete, fully-integrated DevOps application ecosystem with optional seed data populated.


### Available Applications

Currently the project provides:

- [Keycloak](https://www.keycloak.org): Identity Management and Single Sign-on Provider
- [JIRA](https://www.atlassian.com/software/jira): Task Management
- [Jenkins](https://jenkins.io): CI Server
- [GitLab](https://about.gitlab.com): Source Control Management
- [SonarQube](https://www.sonarqube.org): Code Quality Platform


### Available Platform

- **AWS**
    - `baremetal`: Provisioning based on EC2 virtual machines


### Available Configurations

- `dev-standard`: Standard Development environment. Mirrors the Production environment, but at a reduced scale/availability.

### Prerequisites

- [Terraform](https://www.terraform.io)
- [Terragrunt](https://github.com/gruntwork-io/terragrunt)
- [Ansible](https://www.ansible.com)

### Recommended IDE
- [IntelliJ IDEA](https://www.jetbrains.com/idea) + [HCL Language Support](https://plugins.jetbrains.com/plugin/7808-hashicorp-terraform--hcl-language-support)

## Usage

See the USAGE file for each platform for detailed usage guide.

- [AWS](/aws/USAGE.md)
