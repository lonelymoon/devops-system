output "engine" {
  value = var.engine
}

output "engine_version" {
  value = var.engine_version
}

output "host" {
  value = module.rds.db_instance_address
}

output "port" {
  value = module.rds.db_instance_port
}

output "name" {
  value = module.rds.db_instance_name
}

output "username" {
  value = module.rds.db_instance_username
}

output "password" {
  value = module.rds.db_instance_password
  sensitive = true
}
