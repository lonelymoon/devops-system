variable "app_name" {
  type = string
}

variable "vpc_id" {
  type = string
}

variable "subnets" {
  type = list(string)
}

variable "engine" {
  type = string
}

variable "engine_version" {
  type = string
}

variable "major_engine_version" {
  type = string
}

variable "family" {
  type = string
}

variable "instance_class" {
  type = string
}

variable "multi_az" {
  type    = bool
  default = false
}

variable "storage_size" {
  type = number
}

variable "storage_type" {
  type    = string
  default = "gp2"
}

variable "name" {
  type = string
}

variable "port" {
  type = string
}

variable "username" {
  type = string
}

variable "password" {
  type = string
}

variable "tags" {
  type = map(string)
}
