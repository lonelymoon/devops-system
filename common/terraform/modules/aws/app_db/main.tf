module "rds" {
  source  = "terraform-aws-modules/rds/aws"
  version = "~> 3.3"

  identifier          = var.name
  engine              = var.engine
  engine_version      = var.engine_version
  instance_class      = var.instance_class
  allocated_storage   = var.storage_size
  storage_type        = var.storage_type
  storage_encrypted   = false
  publicly_accessible = false
  multi_az            = var.multi_az

  # DB parameter group
  family = var.family

  # DB option group
  major_engine_version = var.major_engine_version

  name     = var.name
  port     = var.port
  username = var.username
  password = var.password

  vpc_security_group_ids = [aws_security_group.db.id]
  subnet_ids             = var.subnets

  maintenance_window              = "Thu:00:00-Thu:03:00"
  backup_window                   = "03:00-06:00"
  backup_retention_period         = 0
  enabled_cloudwatch_logs_exports = []
  skip_final_snapshot             = true

  performance_insights_enabled = false

  tags = var.tags
}

resource "aws_security_group" "db" {
  name   = "${var.app_name}_db"
  vpc_id = data.aws_vpc.devops_systems.id

  ingress {
    from_port = var.port
    to_port   = var.port
    protocol  = "tcp"
    cidr_blocks = [
      data.aws_vpc.devops_systems.cidr_block
    ]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = var.tags
}

data "aws_vpc" "devops_systems" {
  id = var.vpc_id
}
