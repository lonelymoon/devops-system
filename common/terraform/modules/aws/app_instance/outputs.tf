output "app_host" {
  value = local.app_host
}

output "app_url" {
  value = local.app_url
}

output "app_instance" {
  value = aws_instance.app
}

output "app_instance_id" {
  value = aws_instance.app.id
}

output "app_instance_private_ip" {
  value = aws_instance.app.private_ip
}

