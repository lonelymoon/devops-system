resource "aws_instance" "app" {
  ami = var.use_default_ami ? data.aws_ami.amz_linux_2.id : var.ami_id

  instance_type = var.instance_type
  key_name      = var.keypair

  // pick a random subnet
  subnet_id = random_shuffle.subnets.result[0]

  ebs_optimized = false
  vpc_security_group_ids = concat(
    [
      var.vpc_default_security_group_id,
      aws_security_group.app_instance.id
    ],
    var.additional_security_groups
  )

  iam_instance_profile = aws_iam_instance_profile.app_instance.name

  user_data_base64 = data.template_cloudinit_config.app_cloud_config.rendered

  associate_public_ip_address = false
  source_dest_check           = true

  root_block_device {
    volume_type = var.root_block_device_type
    volume_size = var.root_block_device_size
  }

  timeouts {
    create = "15m"
    update = "10m"
    delete = "20m"
  }

  tags = merge(
    var.tags,
    {
      Name = var.app_name
  })
}

resource "random_shuffle" "subnets" {
  input        = var.subnets
  result_count = 1
}

data "aws_ami" "amz_linux_2" {
  owners = [
    "amazon"
  ]
  most_recent = true

  filter {
    name = "name"
    values = [
      "amzn2-ami-hvm-2.0.????????.?-x86_64-gp2"
    ]
  }

  filter {
    name = "state"
    values = [
      "available"
    ]
  }
}

data "template_cloudinit_config" "app_cloud_config" {
  gzip          = true
  base64_encode = true

  part {
    filename     = "init.cfg"
    content_type = "text/cloud-config"
    content      = data.template_file.cloud_init_config.rendered
    merge_type   = "list(append)+dict(recurse_array)+str()"
  }
}

data "template_file" "cloud_init_config" {
  template = file("${path.module}/user_data/00_init.yaml")
  vars = {
    hostname = var.app_name
  }
}

resource "aws_security_group" "app_instance" {
  name   = "${var.app_name}_instance"
  vpc_id = var.vpc_id

  ingress {
    from_port = var.app_ssh_port
    to_port   = var.app_ssh_port
    protocol  = "tcp"
    security_groups = [
      data.aws_security_group.bastion_instance.id
    ]
  }

  ingress {
    from_port = var.app_web_port
    to_port   = var.app_web_port
    protocol  = "tcp"
    security_groups = [
      data.aws_security_group.alb.id
    ]
  }

  ingress {
    from_port = 0
    to_port   = 0
    protocol  = "-1"
    security_groups = [

    ]
  }

  egress {
    from_port = 0
    to_port   = 0
    protocol  = "-1"
    cidr_blocks = [
    "0.0.0.0/0"]
  }

  tags = var.tags
}

resource "aws_lb_target_group" "app_web" {
  name        = var.app_name
  port        = var.app_web_port
  protocol    = "HTTP"
  target_type = "instance"
  vpc_id      = var.vpc_id

  health_check {
    enabled = var.alb_health_check
    path    = var.alb_health_check_path
  }

  tags = var.tags
}

resource "aws_lb_target_group_attachment" "app_web" {
  target_group_arn = aws_lb_target_group.app_web.arn
  target_id        = aws_instance.app.id
  port             = var.app_web_port
}

resource "aws_lb_listener_rule" "app" {
  depends_on = [
    aws_lb_target_group.app_web
  ]

  listener_arn = data.aws_lb_listener.https.arn

  action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.app_web.arn
  }

  condition {
    host_header {
      values = [
      local.app_host]
    }
  }
}

data "aws_lb_listener" "https" {
  load_balancer_arn = var.alb_arn
  port              = 443
}

data "aws_lb" "alb" {
  arn = var.alb_arn
}

resource "aws_iam_instance_profile" "app_instance" {
  name = "${var.app_name}_instance"
  role = module.iam_assumable_role.iam_role_name
}

module "iam_assumable_role" {
  source  = "terraform-aws-modules/iam/aws//modules/iam-assumable-role"
  version = "~> 4.2"

  trusted_role_services = [
    "ec2.amazonaws.com"
  ]

  create_role       = true
  role_name         = "${var.app_name}_instance"
  role_requires_mfa = false

  custom_role_policy_arns = [
    module.iam_policy.arn
  ]

  tags = var.tags
}

module "iam_policy" {
  source  = "terraform-aws-modules/iam/aws//modules/iam-policy"
  version = "~> 4.2"

  name   = "${var.app_name}_instance"
  path   = "/"
  policy = "${data.aws_iam_policy_document.app_instance.json}"
}

data "aws_iam_policy_document" "app_instance" {
  statement {
    sid = "CloudWatchLogsAgent"

    actions = [
      "logs:CreateLogGroup",
      "logs:CreateLogStream",
      "logs:PutLogEvents",
      "logs:DescribeLogStreams"
    ]

    resources = [
      "*",
    ]
  }
}

resource "aws_route53_record" "app_public" {
  zone_id = data.aws_route53_zone.public.zone_id
  name    = "${var.app_name}.${var.dns_domain_name}"
  type    = "A"

  alias {
    name                   = data.aws_lb.alb.dns_name
    zone_id                = data.aws_lb.alb.zone_id
    evaluate_target_health = false
  }
}

data "aws_route53_zone" "public" {
  zone_id = var.dns_public_zone_id
}

resource "aws_route53_record" "app_private_ip" {
  zone_id = data.aws_route53_zone.private.zone_id
  name    = var.app_name
  type    = "A"
  count   = var.dns_private_zone_use_private_ip ? 1 : 0
  ttl     = 300

  records = [
    aws_instance.app.private_ip,
  ]
}

resource "aws_route53_record" "app_private_lb" {
  zone_id = data.aws_route53_zone.private.zone_id
  name    = var.app_name
  type    = "A"
  count   = var.dns_private_zone_use_private_ip ? 0 : 1

  alias {
    evaluate_target_health = false
    name                   = data.aws_lb.alb.dns_name
    zone_id                = data.aws_lb.alb.zone_id
  }
}

resource "aws_route53_record" "app_private_additional_dns" {
  for_each = toset(var.dns_private_zone_additional_hostnames)

  zone_id = data.aws_route53_zone.private.zone_id
  name    = each.value
  type    = "A"
  ttl     = 300

  records = [
    aws_instance.app.private_ip
  ]
}

data "aws_route53_zone" "private" {
  zone_id      = var.dns_private_zone_id
  private_zone = true
}

data "aws_security_group" "bastion_instance" {
  id = var.bastion_security_group
}

data "aws_security_group" "alb" {
  id = var.alb_security_group
}
