locals {
  app_host = "${var.app_name}.${var.dns_domain_name}"
  app_url  = "https://${var.app_name}.${var.dns_domain_name}"
}

variable "app_name" {
  type = string
}

variable "app_version" {
  type = string
}

variable "app_web_port" {
  type = number
}

variable "app_ssh_port" {
  type    = number
  default = 22
}

variable "instance_type" {
  type    = string
  default = "t2.medium"
}

variable "root_block_device_type" {
  type    = string
  default = "gp2"
}

variable "root_block_device_size" {
  type    = number
  default = 10
}

variable "use_default_ami" {
  type    = bool
  default = true
}

variable "ami_id" {
  type    = string
  default = ""
}

variable "vpc_id" {
  type = string
}

variable "vpc_default_security_group_id" {
	type = string
}

variable "additional_security_groups" {
  type    = list(string)
  default = []
}

variable "storage_bucket" {
  type = string
}

variable "alb_arn" {
  type = string
}

variable "alb_security_group" {
  type = string
}

variable "alb_health_check" {
  type    = bool
  default = true
}

variable "alb_health_check_path" {
  type    = string
  default = "/"
}

variable "subnets" {
  type = list(string)
}

variable "keypair" {
  type = string
}

variable "dns_domain_name" {
  type = string
}

variable "dns_public_zone_id" {
  type = string
}

variable "dns_private_zone_use_private_ip" {
  type    = bool
  default = false
  # default = true
}

variable "dns_private_zone_id" {
  type = string
}

variable "dns_private_zone_additional_hostnames" {
  type    = list(string)
  default = []
}

variable "bastion_security_group" {
  type = string
}

variable "tags" {
  type = map(string)
}
