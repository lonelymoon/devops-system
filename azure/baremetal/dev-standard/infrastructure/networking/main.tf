resource "azurerm_virtual_network" "devops-systems" {
  name                = var.vnet_name
  location            = azurerm_resource_group.devops-systems.location
  resource_group_name = azurerm_resource_group.devops-systems.name
  address_space       = [var.vnet_cidr]

  ddos_protection_plan {
    id     = azurerm_network_ddos_protection_plan.devops-systems.id
    enable = var.vnet_ddos_protection_enabled
  }

  tags = var.tags
}
