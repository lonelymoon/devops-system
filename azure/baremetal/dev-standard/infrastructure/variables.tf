variable "resource_group_name" {
  type = string
}

variable "resource_group_location" {
  type = string
}

variable "vnet_name" {
  type = string
}

variable "vnet_cidr" {
  type = string
}

variable "vnet_ddos_protection_enabled" {
  type = bool
}

variable "ddos_protection_plan_name" {
  type = string
}

# variable "bastion_linux_admin_username" {
#   type = string
# }

variable "tags" {
  type = map(string)
}
