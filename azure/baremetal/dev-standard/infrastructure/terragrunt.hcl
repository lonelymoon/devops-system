include {
  path = find_in_parent_folders()
}

locals {
  devops_system_config = yamldecode(file(find_in_parent_folders("devops_system_config.yaml")))
  env_config           = yamldecode(file(find_in_parent_folders("env_config.yaml")))
}

inputs = {
  resource_group_name          = local.devops_system_config.infrastructure.resource_group.name
  resource_group_location      = local.env_config.azure.location
  vnet_name                    = local.devops_system_config.infrastructure.vnet.name
  vnet_cidr                    = local.devops_system_config.infrastructure.vnet.cidr
  vnet_ddos_protection_enabled = local.devops_system_config.infrastructure.vnet.ddos_protection_enabled
  ddos_protection_plan_name    = local.devops_system_config.infrastructure.ddos_protection_plan.name

  tags = merge(
    local.env_config.azure.common_tags,
    {
      Module : basename(get_terragrunt_dir()),
    }
  )
}
