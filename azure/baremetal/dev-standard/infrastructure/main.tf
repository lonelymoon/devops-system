resource "azurerm_resource_group" "devops-systems" {
  name     = var.resource_group_name
  location = var.resource_group_location

  tags = var.tags
}

resource "azurerm_network_security_group" "devops-systems" {
  name                = var.vnet_name
  location            = azurerm_resource_group.devops-systems.location
  resource_group_name = azurerm_resource_group.devops-systems.name

  tags = var.tags
}

resource "azurerm_network_ddos_protection_plan" "devops-systems" {
  name                = var.ddos_protection_plan_name
  location            = azurerm_resource_group.devops-systems.location
  resource_group_name = azurerm_resource_group.devops-systems.name

  tags = var.tags
}

resource "azurerm_virtual_network" "devops-systems" {
  name                = var.vnet_name
  location            = azurerm_resource_group.devops-systems.location
  resource_group_name = azurerm_resource_group.devops-systems.name
  address_space       = [var.vnet_cidr]

  ddos_protection_plan {
    id     = azurerm_network_ddos_protection_plan.devops-systems.id
    enable = var.vnet_ddos_protection_enabled
  }

  tags = var.tags
}

resource "azurerm_subnet" "bastion" {
  name                 = "devops-systems-bastion"
  resource_group_name  = azurerm_resource_group.devops-systems.name
  virtual_network_name = azurerm_virtual_network.devops-systems.name
  address_prefixes = [
    "10.0.0.0/28"
  ]
}

resource "azurerm_subnet" "app" {
  name                 = "devops-systems-app"
  resource_group_name  = azurerm_resource_group.devops-systems.name
  virtual_network_name = azurerm_virtual_network.devops-systems.name
  address_prefixes = [
    "10.0.1.0/24"
  ]
}

resource "azurerm_subnet" "db" {
  name                 = "devops-systems-db"
  resource_group_name  = azurerm_resource_group.devops-systems.name
  virtual_network_name = azurerm_virtual_network.devops-systems.name
  address_prefixes = [
    "10.0.2.0/24"
  ]

  delegation {
    name = "db"
    service_delegation {
      name = "Microsoft.DBforPostgreSQL/serversv2"
    }
  }
}

data "azurerm_client_config" "current" {}

resource "azurerm_key_vault" "devops-systems" {
  name                        = azurerm_resource_group.devops-systems.name
  location                    = azurerm_resource_group.devops-systems.location
  resource_group_name         = azurerm_resource_group.devops-systems.name
  sku_name                    = "standard"
  tenant_id                   = data.azurerm_client_config.current.tenant_id
  soft_delete_retention_days  = 7
  purge_protection_enabled    = false
  enabled_for_disk_encryption = true

  access_policy {
    tenant_id = data.azurerm_client_config.current.tenant_id
    object_id = data.azurerm_client_config.current.object_id

    key_permissions = [
      "Get",
    ]

    secret_permissions = [
      "Get",
    ]

    storage_permissions = [
      "Get",
    ]
  }

  tags = var.tags
}

resource "azurerm_public_ip" "bastion" {
  name                = "devops-systems-bastion"
  location            = azurerm_resource_group.devops-systems.location
  resource_group_name = azurerm_resource_group.devops-systems.name
  sku                 = "Basic"
  allocation_method   = "Dynamic"

  tags = var.tags
}

resource "azurerm_network_interface" "bastion" {
  name                = "devops-systems-bastion"
  location            = azurerm_resource_group.devops-systems.location
  resource_group_name = azurerm_resource_group.devops-systems.name

  ip_configuration {
    name                          = "public"
    primary                       = true
    subnet_id                     = azurerm_subnet.bastion.id
    public_ip_address_id          = azurerm_public_ip.bastion.id
    private_ip_address_allocation = "Dynamic"
  }

  tags = var.tags
}
