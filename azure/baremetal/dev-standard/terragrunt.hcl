locals {
  devops_system_config = yamldecode(file("devops_system_config.yaml"))
  env_config           = yamldecode(file("env_config.yaml"))
}

remote_state {
  backend = "azurerm"

  config = {
    resource_group_name  = local.env_config.terraform.resource_group
    storage_account_name = local.env_config.terraform.storage_account
    container_name       = local.env_config.terraform.container
    key                  = "${local.env_config.terraform.environment}_${path_relative_to_include()}.tfstate"
  }

  generate = {
    path      = "backend.tf"
    if_exists = "overwrite"
  }
}

generate "terraform" {
  path      = "terraform.tf"
  if_exists = "overwrite_terragrunt"
  contents  = file("${get_parent_terragrunt_dir()}/terraform.template.hcl")
}

terraform {
  extra_arguments "debug_logging" {
    commands = [
      "init",
      "plan",
      "apply"
    ]

    env_vars = {
      TF_LOG      = "TRACE"
      TF_LOG_PATH = "${basename(get_terragrunt_dir())}.tf.log"
    }
  }

  extra_arguments "plan_out" {
    commands = [
      "plan",
    ]

    arguments = [
      "-out=${basename(get_terragrunt_dir())}.tfplan"
    ]
  }

  extra_arguments "apply_plan" {
    commands = [
      "apply",
    ]

    arguments = [
      "${basename(get_terragrunt_dir())}.tfplan"
    ]
  }
}
