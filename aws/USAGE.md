# AWS DevOps System Provisioning

## Baremetal

### Authentication

`devops-system` uses Terragrunt + Terraform for infrastructure provisioning, which in turn relies on standard AWS authentication methods to communicate with AWS through its API.

Standard AWS authentication methods include:

- Credentials profile file (`~/.aws/credentials`)
- Environment variables (`AWS_ACCESS_KEY_ID` and `AWS_SECRET_ACCESS_KEY`)
- EC2 Instance Metadata Server

Additionally, MFA needs to be configured when enabled on the AWS account.

A simple way to manage the authentication process is to use [aws-vault](https://github.com/99designs/aws-vault).

### Configuration

There are 3 main configuration file:

- `devops_system_config.yaml`:
    - enables/disables application installation
    - specifies application versions/editions
    - installation details
    - enables/disables application pre-configuration
        - enable/disable SSO
        - pre-install plugins
        - etc.
- `env_config.yaml`:
    - configures AWS region
    - bucket/table name for Terraform state management
    - common resource tags
    - keypair for ssh connection
    - etc.
- `devops_system_seed_data.yaml`:
    - user/group names
    - user/group mappings
    - project names
    - etc.

### Provisioning

#### Architecture
[DevOps Systems Architecture](ARCHITECTURE.svg)

#### Workflow
All Applications follows a common workflow

1. Infrastructure provisioning: Terragrunt
2. Application installation: Ansible (local mode)
3. Application pre-configuration: Ansible (local mode)
4. Application data seeding: Ansible (local mode)


### Known Issues

- JIRA requires manual license input after installation to proceed
- JIRA does not support SAML/OpenID Connect SSO out-of-box
- JIRA does not support configuration through API/Configuration file


### TODO

- [ ] Add Monitoring Solution
