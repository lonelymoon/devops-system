skip = local.devops_system_config[local.app_name].disabled

include {
  path = find_in_parent_folders()
}

dependency "infrastructure" {
  config_path = "../infrastructure"
}

dependency "sso" {
  config_path = "../sso"
}

locals {
  devops_system_config    = yamldecode(file(find_in_parent_folders("devops_system_config.yaml")))
  devops_system_seed_data = yamldecode(file(find_in_parent_folders("devops_system_seed_data.yaml")))
  env_config              = yamldecode(file(find_in_parent_folders("env_config.yaml")))
  app_name                = "nexus"
}

inputs = {
  vpc     = dependency.infrastructure.outputs.vpc
  dns     = dependency.infrastructure.outputs.dns
  alb     = dependency.infrastructure.outputs.alb
  storage = dependency.infrastructure.outputs.storage
  bastion = dependency.infrastructure.outputs.bastion

  app_name             = local.app_name
  app_version          = local.devops_system_config[local.app_name].master.version
  app_preconfiguration = local.devops_system_config[local.app_name].master.preconfiguration
  app_web_port         = local.devops_system_config[local.app_name].master.ports["web"]
  app_seed_data        = local.devops_system_seed_data

  sso_url                     = dependency.sso.outputs.url
  sso_realm                   = dependency.sso.outputs.realm
  sso_realm_url               = dependency.sso.outputs.realm_url
  sso_realm_username          = dependency.sso.outputs.realm_username
  sso_realm_password          = dependency.sso.outputs.realm_password
  sso_realm_saml_endpoint_url = dependency.sso.outputs.realm_saml_metadata_url

  tags = merge(
    local.env_config.aws.common_tags,
    {
      Module : local.app_name,
      Environment : local.env_config.terraform.environment,
    }
  )
}
