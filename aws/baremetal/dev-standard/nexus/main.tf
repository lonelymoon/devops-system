module "nexus_instance" {
  source = "../../../../common/terraform/modules/aws/app_instance"

  app_name     = var.app_name
  app_version  = var.app_version
  app_web_port = var.app_web_port

  additional_security_groups = []

  root_block_device_type = "gp2"
  root_block_device_size = 8

  keypair                = var.aws.keypair
  vpc_id                 = var.vpc.vpc_id
  subnets                = var.vpc.private_subnets
  bastion_security_group = var.bastion.security_group
  alb_arn                = var.alb.alb.arn
  alb_security_group     = var.alb.security_group
  storage_bucket         = var.storage.s3.id
  dns_domain_name        = var.dns.domain_name
  dns_public_zone_id     = var.dns.public_zone.zone_id
  dns_private_zone_id    = var.dns.private_zone.zone_id

  tags = merge(var.tags,
    {
      Component : "app"
  })
}

resource "null_resource" "nexus_installation" {
  triggers = {
    installation = filemd5("${path.module}/ansible/install.yaml")
  }

  depends_on = [
    module.nexus_instance,
  ]

  connection {
    type                = "ssh"
    host                = module.nexus_instance.app_instance_private_ip
    port                = var.app_ssh_port
    user                = var.app_ssh_username
    private_key         = var.bastion.private_key
    bastion_host        = var.bastion.public_host
    bastion_port        = var.bastion.port
    bastion_user        = var.bastion.username
    bastion_private_key = var.bastion.private_key
  }

  provisioner "ansible" {
    defaults {
      become_method = "sudo"
      become_user   = "root"
    }

    plays {
      hosts   = [module.nexus_instance.app_instance_private_ip]
      enabled = true
      verbose = true

      playbook {
        file_path = "${path.module}/ansible/install.yaml"
        tags      = []
      }

      extra_vars = {
        app_name         = var.app_name
        app_version      = var.app_version
        app_url          = module.nexus_instance.app_url
        app_host         = module.nexus_instance.app_host
        app_web_port     = var.app_web_port
        install_url      = local.app_installation.url
        install_group    = local.app_installation.group
        install_user     = local.app_installation.user
        install_home_dir = local.app_installation.home_dir
        install_dir      = local.app_installation.dir
        install_tmp      = local.app_installation.tmp
      }
    }
  }
}

resource "null_resource" "nexus_preconfiguration" {
  triggers = {
    configuration = filemd5("${path.module}/ansible/configure.yaml")
  }

  depends_on = [
    module.nexus_instance,
    null_resource.nexus_installation,
  ]

  connection {
    type                = "ssh"
    host                = module.nexus_instance.app_instance_private_ip
    port                = var.app_ssh_port
    user                = var.app_ssh_username
    private_key         = var.bastion.private_key
    bastion_host        = var.bastion.public_host
    bastion_port        = var.bastion.port
    bastion_user        = var.bastion.username
    bastion_private_key = var.bastion.private_key
  }

  provisioner "ansible" {
    defaults {
      become_method = "sudo"
      become_user   = "root"
    }

    plays {
      hosts   = [module.nexus_instance.app_instance_private_ip]
      enabled = var.app_preconfiguration.enabled
      verbose = true

      playbook {
        file_path = "${path.module}/ansible/configure.yaml"
        tags      = []
      }

      extra_vars = {
        app_name         = var.app_name
        app_version      = var.app_version
        app_url          = module.nexus_instance.app_url
        app_host         = module.nexus_instance.app_host
        app_web_port     = var.app_web_port
        install_url      = local.app_installation.url
        install_group    = local.app_installation.group
        install_user     = local.app_installation.user
        install_home_dir = local.app_installation.home_dir
        install_dir      = local.app_installation.dir
        install_tmp      = local.app_installation.tmp

        sso_enabled                 = var.app_preconfiguration.sso.enabled
        sso_url                     = var.sso_url
        sso_realm                   = var.sso_realm
        sso_realm_url               = var.sso_realm_url
        sso_realm_username          = var.sso_realm_username
        sso_realm_password          = var.sso_realm_password
        sso_realm_saml_endpoint_url = var.sso_realm_saml_endpoint_url
        sso_client_name             = var.app_name

        initial_username = var.app_preconfiguration.initial_username
        initial_password = var.app_preconfiguration.initial_password
      }
    }
  }
}
