- hosts: all
  gather_facts: yes
  become: yes
  handlers:
    - include: handlers.yaml
  tasks:
    - name: Ansible Setup
      include_tasks: "{{ playbook_dir }}/subtasks/ansible_dependencies.yaml"

    - name: Pre-Configuration - Initial Credentials
      block:
        - name: Pre-configuration - Initial Credentials - Check if password is auto-generated
          stat:
            path: "{{ install_home_dir }}/admin.password"
          register: auto_generated_password_file

        - name: Pre-configuration - Initial Credentials - Retrieve auto-generated password
          slurp:
            src: "{{ install_home_dir }}/admin.password"
          register: auto_generated_password
          when: auto_generated_password_file.stat.exists

        - name: Pre-configuration - Initial Credentials - Change initial password
          uri:
            url: "http://localhost:{{ app_web_port }}/service/rest/beta/security/users/{{ initial_username }}/change-password"
            user: "{{ initial_username }}"
            password: "{{ auto_generated_password['content'] | b64decode }}"
            force_basic_auth: yes
            method: PUT
            headers:
              Accept: application/json
              Content-Type: text/plain
            body: "{{ initial_password }}"
            status_code: 204
          when: auto_generated_password_file.stat.exists

    - name: Pre-Configuration - SSO
      when: sso_enabled | bool
      block:
        - name: Pre-configuration - SSO - Generate Client Secret
          set_fact:
            sso_client_secret: "{{ app_url | to_uuid }}"

        - name: Pre-configuration - SSO - Create SSO client
          keycloak_client:
            auth_keycloak_url: "{{ sso_url }}/auth"
            auth_realm: "{{ sso_realm }}"
            auth_username: "{{ sso_realm_username }}"
            auth_password: "{{ sso_realm_password }}"
            realm: "{{ sso_realm }}"
            name: "{{ sso_client_name }}"
            client_id: "{{ sso_client_name }}"
            secret: "{{ sso_client_secret }}"
            protocol: openid-connect
            client_authenticator_type: client-secret
            enabled: yes
            root_url: "{{ app_url }}"
            redirect_uris: "/*"
#            attributes:
#              request.object.signature.alg: RS256
            protocol_mappers:
              - name: username
                protocol: openid-connect
                protocolMapper: oidc-usermodel-property-mapper
                config:
                  user.attribute: username
                  attribute.name: login
                  attribute.nameformat: Basic
                  single: false
                consentRequired: false

              - name: fullname
                protocol: openid-connect
                protocolMapper: oidc-full-name-mapper
                config:
                  id.token.claim: true
                  access.token.claim: true
                  userinfo.token.claim: true
                consentRequired: false

              - name: email
                protocol: openid-connect
                protocolMapper: oidc-usermodel-property-mapper
                config:
                  user.attribute: email
                  attribute.name: email
                  attribute.nameformat: Basic
                  single: false
                consentRequired: false

              - name: groups
                protocol: openid-connect
                protocolMapper: oidc-group-membership-mapper
                config:
                  attribute.name: groups
                  attribute.nameformat: Basic
                  single: false
                  full.path: false
                consentRequired: false
            state: present
          register: sso_client

        - name: Pre-configuration - SSO - Read current Configuration-as-Code
          slurp:
            src: "{{ casc_file }}"
          register: casc

        - name: Pre-configuration - SSO - Delete current Configuration-as-Code
          file:
            path: "{{ casc_file }}"
            state: absent

        - name: Pre-configuration - SSO - Generate SSO Configuration-as-Code
          set_fact:
            empty_security_casc:
              jenkins:
                securityRealm:
            current_casc: "{{ casc.content | b64decode | from_yaml }}"
            sso_casc:
              jenkins:
                securityRealm:
                  oic:
                    automanualconfigure: auto
                    wellKnownOpenIDConfigurationUrl: "{{ sso_realm_openid_endpoint_url }}"
                    clientId: "{{ sso_client_name }}"
                    clientSecret: "{{ sso_client_secret }}"
                    disableSslVerification: false
                    logoutFromOpenidProvider: true
                    postLogoutRedirectUrl: "{{ app_url }}"
                    userNameField: preferred_username
                    fullNameFieldName: fullname
                    emailFieldName: email
                    groupsFieldName: groups
                    escapeHatchEnabled: true
                    escapeHatchUsername: "{{ initial_username }}"
                    escapeHatchSecret: "{{ initial_password }}"

        - name: Pre-configuration - SSO - Generate Updated Configuration-as-Code
          set_fact:
            updated_casc: "{{ current_casc | combine(empty_security_casc, recursive=True) | combine(sso_casc, recursive=True) }}"

        - name: Pre-configuration - SSO - Write SSO Configuration-as-Code
          copy:
            content: "{{ updated_casc | to_nice_yaml }}"
            dest: "{{ casc_dir }}/casc.yaml"
            group: "{{ install_group }}"
            owner: "{{ install_user }}"
          notify:
            - restart
