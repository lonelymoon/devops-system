module "elk_instance" {
  source = "../../../../common/terraform/modules/aws/app_instance"

  app_name     = var.app_name
  app_version  = var.app_version
  app_web_port = var.app_kibana_port

  additional_security_groups = [
    aws_security_group.elasticsearch.id,
    aws_security_group.logstash.id,
    aws_security_group.kibana.id,
  ]

  use_default_ami = false
  ami_id          = data.aws_ami.opendistro_elasticsearch.id

  root_block_device_type = "gp2"
  root_block_device_size = 8

  keypair                       = var.aws.keypair
  vpc_id                        = var.vpc.vpc_id
  vpc_default_security_group_id = var.vpc.default_security_group_id
  subnets                       = var.vpc.private_subnets
  bastion_security_group        = var.bastion.security_group
  alb_arn                       = var.alb.alb.arn
  alb_security_group            = var.alb.security_group
  //	alb_health_check       = true
  storage_bucket                  = var.storage.s3.id
  dns_domain_name                 = var.dns.domain_name
  dns_public_zone_id              = var.dns.public_zone.zone_id
  dns_private_zone_id             = var.dns.private_zone.zone_id
  dns_private_zone_use_private_ip = true
  dns_private_zone_additional_hostnames = [
    "elasticsearch",
    "logstash",
    "kibana",
  ]


  tags = merge(var.tags,
    {
      Component : "elk"
  })
}

data "aws_ami" "opendistro_elasticsearch" {
  owners = [
    "745719312218" # Open Distro Elasticsearch (maybe)
  ]
  most_recent = true

  filter {
    name = "name"
    values = [
      "Open Distro for Elasticsearch-${var.app_version}-*"
    ]
  }

  filter {
    name = "state"
    values = [
      "available"
    ]
  }
}

resource "aws_security_group" "elasticsearch" {
  name   = "elasticsearch_instance"
  vpc_id = var.vpc.vpc_id

  ingress {
    from_port = var.app_elasticsearch_port
    to_port   = var.app_elasticsearch_port
    protocol  = "tcp"
    security_groups = [
      var.vpc.default_security_group_id
    ]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = var.tags
}

resource "aws_security_group" "logstash" {
  name   = "logstash_instance"
  vpc_id = var.vpc.vpc_id

  ingress {
    from_port = var.app_logstash_port
    to_port   = var.app_logstash_port
    protocol  = "tcp"
    security_groups = [
      var.vpc.default_security_group_id
    ]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = var.tags
}

resource "aws_security_group" "kibana" {
  name   = "kibana_instance"
  vpc_id = var.vpc.vpc_id

  ingress {
    from_port = var.app_kibana_port
    to_port   = var.app_kibana_port
    protocol  = "tcp"
    security_groups = [
      var.vpc.default_security_group_id
    ]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = var.tags
}

resource "null_resource" "elasticsearch_installation" {
  triggers = {
    installation = filemd5("${path.module}/ansible/install.yaml")
    id           = uuid()
  }

  depends_on = [
    module.elk_instance,
  ]

  connection {
    type                = "ssh"
    host                = module.elk_instance.app_instance_private_ip
    port                = var.app_ssh_port
    user                = var.app_ssh_username
    private_key         = var.bastion.private_key
    bastion_host        = var.bastion.public_host
    bastion_port        = var.bastion.port
    bastion_user        = var.bastion.username
    bastion_private_key = var.bastion.private_key
  }

  provisioner "ansible" {
    defaults {
      become_method = "sudo"
      become_user   = "root"
    }

    plays {
      hosts   = [module.elk_instance.app_instance_private_ip]
      enabled = true
      verbose = true

      playbook {
        file_path = "${path.module}/ansible/install.yaml"
        tags      = []
      }

      extra_vars = {
        app_name    = var.app_name
        app_version = var.app_version
        app_url     = module.elk_instance.app_url
        app_host    = module.elk_instance.app_host

        app_elasticsearch_url  = module.elk_instance.app_url
        app_elasticsearch_host = module.elk_instance.app_host
        app_elasticsearch_port = var.app_elasticsearch_port
        app_logstash_url       = module.elk_instance.app_url
        app_logstash_host      = module.elk_instance.app_host
        app_logstash_port      = var.app_logstash_port
        app_kibana_url         = module.elk_instance.app_url
        app_kibana_host        = module.elk_instance.app_host
        app_kibana_port        = var.app_kibana_port


        install_url   = local.app_installation.url
        install_group = local.app_installation.group
        install_user  = local.app_installation.user
        install_dir   = local.app_installation.dir
        install_tmp   = local.app_installation.tmp

        sso_enabled                     = var.app_preconfiguration.sso.enabled
        sso_url                         = var.sso_url
        sso_realm                       = var.sso_realm
        sso_realm_url                   = var.sso_realm_url
        sso_realm_username              = var.sso_realm_username
        sso_realm_password              = var.sso_realm_password
        sso_realm_openid_well_known_url = var.sso_realm_openid_well_known_url
        sso_realm_openid_endpoint_url   = var.sso_realm_openid_endpoint_url
        sso_client_name                 = var.app_name
      }
    }
  }
}
