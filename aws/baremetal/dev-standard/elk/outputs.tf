output "host" {
	value = module.elk_instance.app_host
}

output "url" {
	value = module.elk_instance.app_url
}

output "elasticsearch_port" {
	value = var.app_elasticsearch_port
}

output "logstash_port" {
	value = var.app_logstash_port
}

output "instance_private_ip" {
	value = module.elk_instance.app_instance_private_ip
}
