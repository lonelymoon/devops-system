locals {
	app_installation = {
		url   = "https://d3g5vo6xdbdb9a.cloudfront.net/yum/opendistroforelasticsearch-artifacts.repo"
		group = var.app_name
		user  = var.app_name
		dir   = "/usr/share/elasticsearch"
		tmp   = "/opt/${var.app_name}/tmp"
	}
}

variable "aws" {
	type = object({
		region : string,
		keypair : string,
		common_tags : map(string),
	})
}

variable "vpc" {
	type = object({
		vpc_id : string,
		vpc_cidr_block : string,
		default_security_group_id : string,
		private_subnets : list(string),
		intra_subnets : list(string),
	})
}

variable "alb" {
	type = object({
		alb : object({
			arn : string,
		}),
		security_group : string,
	})
}

variable "dns" {
	type = object({
		domain_name : string,
		public_zone : object({
			zone_id : string,
		}),
		private_zone : object({
			zone_id : string,
		})
	})
}

variable "storage" {
	type = object({
		s3 : object({
			arn : string,
			id : string,
			bucket : string,
			bucket_domain_name : string,
			bucket_regional_domain_name : string,
		})
	})
}

variable "bastion" {
	type = object({
		security_group : string,
		public_host : string,
		port : number,
		username : string,
		private_key : string,
	})
}

variable "instance_type" {
	type    = string
	default = "t2.medium"
}

variable "tags" {
	type = map(string)
}

variable "app_name" {
	type = string
}

variable "app_version" {
	type = string
}

variable "app_elasticsearch_port" {
	type = number
}

variable "app_logstash_port" {
	type = number
}

variable "app_kibana_port" {
	type = number
}

variable "app_ssh_port" {
	type    = number
	default = 22
}

variable "app_ssh_username" {
	type    = string
	default = "ec2-user"
}

variable "app_preconfiguration" {
	type = object({
		enabled : bool,
		sso : object({
			enabled : bool,
		}),
	})
}

variable "sso_url" {
	type = string
}

variable "sso_realm" {
	type = string
}

variable "sso_realm_url" {
	type = string
}

variable "sso_realm_username" {
	type = string
}

variable "sso_realm_password" {
	type = string
}

variable "sso_realm_openid_well_known_url" {
	type = string
}

variable "sso_realm_openid_endpoint_url" {
	type = string
}
