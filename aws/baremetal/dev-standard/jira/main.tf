module "jira_instance" {
  source = "../../../../common/terraform/modules/aws/app_instance"

  app_name     = var.app_name
  app_version  = var.app_version
  app_web_port = var.app_web_port

  additional_security_groups = []

  root_block_device_type = "gp2"
  root_block_device_size = 8

  keypair                       = var.aws.keypair
  vpc_id                        = var.vpc.vpc_id
  vpc_default_security_group_id = var.vpc.default_security_group_id
  subnets                       = var.vpc.private_subnets
  bastion_security_group        = var.bastion.security_group
  alb_arn                       = var.alb.alb.arn
  alb_security_group            = var.alb.security_group
  storage_bucket                = var.storage.s3.id
  dns_domain_name               = var.dns.domain_name
  dns_public_zone_id            = var.dns.public_zone.zone_id
  dns_private_zone_id           = var.dns.private_zone.zone_id

  tags = merge(var.tags,
    {
      Component : "app"
  })
}

resource "null_resource" "jira_installation" {
  triggers = {
    installation = filemd5("${path.module}/ansible/install.yaml")
  }

  connection {
    type                = "ssh"
    host                = module.jira_instance.app_instance_private_ip
    port                = var.app_ssh_port
    user                = "ec2-user"
    private_key         = var.bastion.private_key
    bastion_host        = var.bastion.public_host
    bastion_port        = var.bastion.port
    bastion_user        = var.bastion.username
    bastion_private_key = var.bastion.private_key
  }

  provisioner "ansible" {
    defaults {
      become_method = "sudo"
      become_user   = "root"
    }

    plays {
      hosts   = [module.jira_instance.app_instance_private_ip]
      enabled = true
      verbose = true

      playbook {
        file_path = "${path.module}/ansible/install.yaml"
        tags      = []
      }

      extra_vars = {
        app_name         = var.app_name
        app_version      = var.app_version
        app_url          = module.jira_instance.app_url
        app_host         = module.jira_instance.app_host
        install_url      = local.app_installation.url
        install_group    = local.app_installation.group
        install_user     = local.app_installation.user
        install_home_dir = local.app_installation.home_dir
        install_dir      = local.app_installation.dir
        install_tmp      = local.app_installation.tmp
        db_engine        = module.jira_db.engine
        db_host          = module.jira_db.host
        db_port          = module.jira_db.port
        db_name          = module.jira_db.name
        db_username      = module.jira_db.username
        db_password      = module.jira_db.password
      }
    }
  }
}



resource "null_resource" "jira_preconfiguration" {
  depends_on = [
    null_resource.jira_installation
  ]

  triggers = {
    app_preconfiguration = jsonencode(var.app_preconfiguration)
    configuration        = filemd5("${path.module}/ansible/configure.yaml")
  }

  connection {
    type                = "ssh"
    host                = module.jira_instance.app_instance_private_ip
    port                = var.app_ssh_port
    user                = "ec2-user"
    private_key         = var.bastion.private_key
    bastion_host        = var.bastion.public_host
    bastion_port        = var.bastion.port
    bastion_user        = var.bastion.username
    bastion_private_key = var.bastion.private_key
  }

  provisioner "ansible" {
    defaults {
      become_method = "sudo"
      become_user   = "root"
    }

    plays {
      hosts   = [module.jira_instance.app_instance_private_ip]
      enabled = var.app_preconfiguration.enabled
      verbose = true

      playbook {
        file_path = "${path.module}/ansible/configure.yaml"
        tags      = []
      }

      extra_vars = {
        app_name         = var.app_name
        install_home_dir = local.app_installation.home_dir
        install_dir      = local.app_installation.dir

        app_license = var.app_preconfiguration.license
        db_engine   = module.jira_db.engine
        db_host     = module.jira_db.host
        db_port     = module.jira_db.port
        db_name     = module.jira_db.name
        db_username = module.jira_db.username
        db_password = module.jira_db.password

        initial_username = var.app_preconfiguration.initial_username
        initial_password = var.app_preconfiguration.initial_password
      }
    }
  }
}

module "jira_db" {
  source = "../../../../common/terraform/modules/aws/app_db"

  app_name             = var.app_name
  engine               = var.db_engine
  engine_version       = var.db_engine_version
  major_engine_version = var.db_major_engine_version
  family               = var.db_family
  instance_class       = var.db_instance_class
  multi_az             = var.db_multi_az
  storage_size         = var.db_storage_size
  storage_type         = var.db_storage_type
  name                 = var.db_name
  port                 = var.db_port
  username             = var.db_username
  password             = var.db_password

  vpc_id  = var.vpc.vpc_id
  subnets = var.vpc.intra_subnets

  tags = merge(var.tags,
    {
      Component : "db",
  })
}
