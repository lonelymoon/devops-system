locals {
	edition_install_url = {
		community  = "https://binaries.sonarsource.com/Distribution/sonarqube/sonarqube-${var.app_version}.zip"
		developer  = "https://binaries.sonarsource.com/CommercialDistribution/sonarqube-developer/sonarqube-developer-${var.app_version}.zip"
		enterprise = "https://binaries.sonarsource.com/CommercialDistribution/sonarqube-enterprise/sonarqube-enterprise-${var.app_version}.zip"
		datacenter = "https://binaries.sonarsource.com/CommercialDistribution/sonarqube-datacenter/sonarqube-datacenter-${var.app_version}.zip"
	}

	app_installation = {
		url   = local.edition_install_url[var.app_edition]
		group = var.app_name
		user  = var.app_name
		dir   = "/opt/${var.app_name}/install"
		tmp   = "/opt/${var.app_name}/tmp"
	}

	//  ansible_playbook_dir  = "/opt/ansible"
	//  ansible_log_dir       = "/opt/ansible/logs"
	//  ansible_config_file   = "ansible.cfg"
	//  ansible_log_file      = "ansible.log"
}

variable "aws" {
	type = object({
		region : string,
		keypair : string,
		common_tags : map(string),
	})
}

variable "vpc" {
	type = object({
		vpc_id : string,
		vpc_cidr_block : string,
		default_security_group_id : string,
		private_subnets : list(string),
		intra_subnets : list(string),
	})
}

variable "alb" {
	type = object({
		alb : object({
			arn : string,
		}),
		security_group : string,
	})
}

variable "dns" {
	type = object({
		domain_name : string,
		public_zone : object({
			zone_id : string,
		}),
		private_zone : object({
			zone_id : string,
		})
	})
}

variable "storage" {
	type = object({
		s3 : object({
			arn : string,
			id : string,
			bucket : string,
			bucket_domain_name : string,
			bucket_regional_domain_name : string,
		})
	})
}

variable "bastion" {
	type = object({
		security_group : string,
		public_host : string,
		port : number,
		username : string,
		private_key: string,
	})
}

variable "instance_type" {
	type    = string
	default = "t2.medium"
}

variable "tags" {
	type = map(string)
}

variable "app_name" {
	type = string
}

variable "app_version" {
	type = string
}

variable "app_edition" {
	type = string
}

variable "app_web_port" {
	type = number
}

variable "app_ssh_port" {
	type    = number
	default = 22
}

variable "app_ssh_username" {
	type    = string
	default = "ec2-user"
}

variable "app_preconfiguration" {
	type = object({
		enabled : bool,
		sso: object({
			enabled: bool,
		}),
		plugins: list(object({
			name: string,
			url: string,
			extra_locations: list(string),
		}))
	})
}

variable "app_seed_data" {
	type = object({
		groups: list(object({
			name: string,
			admin: bool,
		})),
		users: list(object({
			username: string,
			password: string,
			first_name: string,
			last_name: string,
			email: string,
		})),
		group_users_mapping: list(object({
			group: string,
			users: list(string),
		})),
	})
}

variable "sso_url" {
	type = string
}

variable "sso_realm" {
	type = string
}

variable "sso_realm_url" {
	type = string
}

variable "sso_realm_username" {
	type = string
}

variable "sso_realm_password" {
	type = string
}

variable "sso_realm_saml_metadata_url" {
	type = string
}

variable "db_engine" {
	type = string
}

variable "db_engine_version" {
	type = string
}

variable "db_major_engine_version" {
	type = string
}

variable "db_family" {
	type = string
}

variable "db_instance_class" {
	type    = string
	default = "db.m4.large"
}

variable "db_multi_az" {
	type    = bool
	default = false
}

variable "db_storage_size" {
	type    = number
	default = 5
}

variable "db_storage_type" {
	type    = string
	default = "gp2"
}

variable "db_name" {
	type = string
}

variable "db_port" {
	type = string
}

variable "db_username" {
	type = string
}

variable "db_password" {
	type = string
}

variable "monitoring_logstash_host" {
	type = string
}

variable "monitoring_logstash_port" {
	type = number
}
