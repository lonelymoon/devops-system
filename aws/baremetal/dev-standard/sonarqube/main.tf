module "sonarqube_instance" {
  source = "../../../../common/terraform/modules/aws/app_instance"

  app_name     = var.app_name
  app_version  = var.app_version
  app_web_port = var.app_web_port

  additional_security_groups = []

  root_block_device_type = "gp2"
  root_block_device_size = 8

  keypair                       = var.aws.keypair
  vpc_id                        = var.vpc.vpc_id
  vpc_default_security_group_id = var.vpc.default_security_group_id
  subnets                       = var.vpc.private_subnets
  bastion_security_group        = var.bastion.security_group
  alb_arn                       = var.alb.alb.arn
  alb_security_group            = var.alb.security_group
  storage_bucket                = var.storage.s3.id
  dns_domain_name               = var.dns.domain_name
  dns_public_zone_id            = var.dns.public_zone.zone_id
  dns_private_zone_id           = var.dns.private_zone.zone_id

  tags = merge(
    var.tags,
    {
      Component : "app"
    }
  )
}

resource "null_resource" "sonarqube_installation" {
  depends_on = [
    module.sonarqube_instance,
    module.sonarqube_db,
  ]

  triggers = {
    installation                  = filemd5("${path.module}/ansible/install.yaml")
    monitoring_filebeat_template  = filemd5("${path.module}/ansible/templates/beats/filebeat/filebeat.yml")
    monitoring_auditbeat_template = filemd5("${path.module}/ansible/templates/beats/auditbeat/auditbeat.yml")
  }

  connection {
    type                = "ssh"
    host                = module.sonarqube_instance.app_instance_private_ip
    port                = var.app_ssh_port
    user                = var.app_ssh_username
    private_key         = var.bastion.private_key
    bastion_host        = var.bastion.public_host
    bastion_port        = var.bastion.port
    bastion_user        = var.bastion.username
    bastion_private_key = var.bastion.private_key
  }

  provisioner "ansible" {
    defaults {
      become_method = "sudo"
      become_user   = "root"
    }

    plays {
      hosts   = [module.sonarqube_instance.app_instance_private_ip]
      enabled = true
      verbose = true

      playbook {
        file_path = "${path.module}/ansible/install.yaml"
        roles_path = [
          "${abspath(path.root)}/../../../../common/ansible/roles"
        ]
        tags = []
      }

      extra_vars = {
        app_name      = var.app_name
        app_version   = var.app_version
        app_edition   = var.app_edition
        app_url       = module.sonarqube_instance.app_url
        install_url   = local.app_installation.url
        install_group = local.app_installation.group
        install_user  = local.app_installation.user
        install_dir   = local.app_installation.dir
        install_tmp   = local.app_installation.tmp
        db_engine     = module.sonarqube_db.engine
        db_host       = module.sonarqube_db.host
        db_port       = module.sonarqube_db.port
        db_name       = module.sonarqube_db.name
        db_username   = module.sonarqube_db.username
        db_password   = module.sonarqube_db.password

        monitoring_logstash_host = var.monitoring_logstash_host
        monitoring_logstash_port = var.monitoring_logstash_port
      }
    }
  }
}

resource "null_resource" "sonarqube_preconfiguration" {
  depends_on = [
    null_resource.sonarqube_installation
  ]

  triggers = {
    app_preconfiguration = jsonencode(var.app_preconfiguration)
    configuration        = filemd5("${path.module}/ansible/configure.yaml")
  }

  connection {
    type                = "ssh"
    host                = module.sonarqube_instance.app_instance_private_ip
    port                = var.app_ssh_port
    user                = var.app_ssh_username
    private_key         = var.bastion.private_key
    bastion_host        = var.bastion.public_host
    bastion_port        = var.bastion.port
    bastion_user        = var.bastion.username
    bastion_private_key = var.bastion.private_key
  }

  provisioner "ansible" {
    defaults {
      become_method = "sudo"
      become_user   = "root"
    }

    plays {
      hosts = [
        module.sonarqube_instance.app_instance_private_ip
      ]
      enabled = var.app_preconfiguration.enabled
      verbose = true

      playbook {
        file_path  = "${path.module}/ansible/configure.yaml"
        roles_path = []
        tags       = []
      }

      extra_vars = {
        install_dir   = local.app_installation.dir
        install_tmp   = local.app_installation.tmp
        install_group = local.app_installation.group
        install_user  = local.app_installation.user

        app_name     = var.app_name
        app_url      = module.sonarqube_instance.app_url
        app_web_port = var.app_web_port

        sso_enabled                 = var.app_preconfiguration.sso.enabled
        sso_url                     = var.sso_url
        sso_realm                   = var.sso_realm
        sso_realm_url               = var.sso_realm_url
        sso_realm_username          = var.sso_realm_username
        sso_realm_password          = var.sso_realm_password
        sso_realm_saml_metadata_url = var.sso_realm_saml_metadata_url
        sso_client_name             = var.app_name

        groups_json               = jsonencode(var.app_seed_data.groups)
        users_json                = jsonencode(var.app_seed_data.users)
        groups_users_mapping_json = jsonencode(var.app_seed_data.group_users_mapping)
        plugins_json              = jsonencode(var.app_preconfiguration.plugins)
      }
    }
  }
}

module "sonarqube_db" {
  source = "../../../../common/terraform/modules/aws/app_db"

  app_name             = var.app_name
  engine               = var.db_engine
  engine_version       = var.db_engine_version
  major_engine_version = var.db_major_engine_version
  family               = var.db_family
  instance_class       = var.db_instance_class
  multi_az             = var.db_multi_az
  storage_size         = var.db_storage_size
  storage_type         = var.db_storage_type
  name                 = var.db_name
  port                 = var.db_port
  username             = var.db_username
  password             = var.db_password

  vpc_id  = var.vpc.vpc_id
  subnets = var.vpc.intra_subnets

  tags = merge(
    var.tags,
    {
      Component : "db",
    }
  )
}
