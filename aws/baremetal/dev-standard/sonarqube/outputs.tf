output "host" {
  value = module.sonarqube_instance.app_host
}

output "url" {
  value = module.sonarqube_instance.app_url
}

output "instance_private_ip" {
	value = module.sonarqube_instance.app_instance_private_ip
}
