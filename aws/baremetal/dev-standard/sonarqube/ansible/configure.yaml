- hosts: all
  gather_facts: yes
  become: yes
  handlers:
    - include: handlers.yaml
  vars:
    initial_username: admin
    initial_password: admin
    plugins: "{{ plugins_json | from_json }}"
    group_names: "{{ groups_json | from_json }}"
    groups_users_mapping: "{{ groups_users_mapping_json | from_json }}"
  tasks:
    - name: Ansible Setup
      include_tasks: "{{ playbook_dir }}/subtasks/ansible_dependencies.yaml"

    - name: Preconfiguration - Plugins
      when: plugins | length > 0
      include: "{{ playbook_dir }}/subtasks/sonarqube_plugins.yaml"
      with_items: "{{ plugins }}"
      loop_control:
        loop_var: plugin
      notify:
        - restart

    - name: Preconfiguration - SSO
      when: sso_enabled | bool
      block:
        - name: Pre-configuration - SSO - Create SSO client
          keycloak_client:
            auth_keycloak_url: "{{ sso_url }}/auth"
            auth_realm: "{{ sso_realm }}"
            auth_username: "{{ sso_realm_username }}"
            auth_password: "{{ sso_realm_password }}"
            realm: "{{ sso_realm }}"
            name: "{{ sso_client_name }}"
            client_id: "{{ sso_client_name }}"
            protocol: saml
            client_authenticator_type: client-secret
            enabled: yes
            root_url: "{{ app_url }}"
            redirect_uris: "{{ app_url }}/oauth2/callback/saml"
            attributes:
              saml.client.signature: false
              saml.signature.algorithm: RSA_SHA512
            protocol_mappers:
              - name: login
                protocol: saml
                protocolMapper: saml-user-property-mapper
                config:
                  user.attribute: username
                  attribute.name: login
                  attribute.nameformat: Basic
                  single: false
                consentRequired: false
              - name: name
                protocol: saml
                protocolMapper: saml-user-property-mapper
                config:
                  user.attribute: username
                  attribute.name: name
                  attribute.nameformat: Basic
                  single: false
                consentRequired: false
              - name: email
                protocol: saml
                protocolMapper: saml-user-property-mapper
                config:
                  user.attribute: email
                  attribute.name: email
                  attribute.nameformat: Basic
                  single: false
                consentRequired: false
              - name: groups
                protocol: saml
                protocolMapper: saml-group-membership-mapper
                config:
                  attribute.name: groups
                  attribute.nameformat: Basic
                  single: false
                  full.path: false
                consentRequired: false
            state: present
          register: sso_client

        - name: Pre-configuration - SSO - Retrieve SAML metadata
          uri:
            url: "{{ sso_realm_saml_metadata_url }}"
            method: GET
            return_content: yes
          register: saml_metadata

        - name: Pre-configuration - SSO - Extract SAML providerId
          xml:
            xmlstring: "{{ saml_metadata.content }}"
            xpath: /ns:EntitiesDescriptor/ns:EntityDescriptor
            content: attribute
            namespaces:
              ns: urn:oasis:names:tc:SAML:2.0:metadata
              dsig: http://www.w3.org/2000/09/xmldsig#
          register: xmlresp

        - name: Pre-configuration - SSO - Set SAML providerId value
          set_fact:
            providerId: '{{ xmlresp.matches[0]["{urn:oasis:names:tc:SAML:2.0:metadata}EntityDescriptor"].entityID }}'

        - name: Pre-configuration - SSO - Extract SAML loginUrl
          xml:
            xmlstring: "{{ saml_metadata.content }}"
            xpath: /ns:EntitiesDescriptor/ns:EntityDescriptor/ns:IDPSSODescriptor/ns:SingleSignOnService[@Binding='urn:oasis:names:tc:SAML:2.0:bindings:HTTP-POST']
            content: attribute
            namespaces:
              ns: urn:oasis:names:tc:SAML:2.0:metadata
              dsig: http://www.w3.org/2000/09/xmldsig#
          register: xmlresp

        - name: Pre-configuration - SSO - Set SAML loginUrl value
          set_fact:
            loginUrl: '{{ xmlresp.matches[0]["{urn:oasis:names:tc:SAML:2.0:metadata}SingleSignOnService"].Location }}'

        - name: Pre-configuration - SSO - Extract SAML certificate
          xml:
            xmlstring: "{{ saml_metadata.content }}"
            xpath: /ns:EntitiesDescriptor/ns:EntityDescriptor/ns:IDPSSODescriptor/ns:KeyDescriptor/dsig:KeyInfo/dsig:X509Data/dsig:X509Certificate
            content: text
            namespaces:
              ns: urn:oasis:names:tc:SAML:2.0:metadata
              dsig: http://www.w3.org/2000/09/xmldsig#
          register: xmlresp

        - name: Pre-configuration - SSO - Set SAML certificate value
          set_fact:
            certificate: '{{ xmlresp.matches[0]["{http://www.w3.org/2000/09/xmldsig#}X509Certificate"] }}'

        - name: Pre-configuration - SSO - Generate SSO configuration
          set_fact:
            sso_configuration:
              sonar.core.serverBaseURL: "{{ app_url }}"
              sonar.auth.saml.applicationId: "{{ sso_client_name }}"
              sonar.auth.saml.providerName: SAML
              sonar.auth.saml.providerId: "{{ providerId }}"
              sonar.auth.saml.loginUrl: "{{ loginUrl }}"
              sonar.auth.saml.certificate.secured: "{{ certificate }}"
              sonar.auth.saml.user.login: login
              sonar.auth.saml.user.name: name
              sonar.auth.saml.user.email: email
              sonar.auth.saml.group.name: groups
              sonar.auth.saml.enabled: !!str true

        - name: Pre-configuration - SSO - Set SSO configuration
          uri:
            url: "http://localhost:{{ app_web_port }}/api/system/health"
            user: "{{ initial_username }}"
            password: "{{ initial_password }}"
            method: GET
            force_basic_auth: yes
          register: health_status
          retries: 10
          delay: 10
          until: health_status.status == 200 and health_status.json['health'] == 'GREEN'

        - name: Pre-configuration - SSO - Set SSO configuration
          uri:
            url: "http://localhost:{{ app_web_port }}/api/settings/set"
            user: "{{ initial_username }}"
            password: "{{ initial_password }}"
            method: POST
            force_basic_auth: yes
            body_format: form-urlencoded
            body:
              key: "{{ item.key }}"
              value: "{{ item.value }}"
            status_code: 204
          with_dict: "{{ sso_configuration }}"

        - include: "{{ playbook_dir }}/subtasks/sonarqube_groups.yaml"
          loop: "{{ group_names | flatten(levels=1) }}"
          loop_control:
            loop_var: group

        - name: Pre-configuration - SSO - Assign Permissions
          uri:
            url: "http://localhost:{{ app_web_port }}/api/permissions/add_group"
            user: "{{ initial_username }}"
            password: "{{ initial_password }}"
            method: POST
            force_basic_auth: yes
            body_format: form-urlencoded
            body:
              groupName: "{{ group.name }}"
              permission: admin
            status_code: 204
          with_items: "{{ group_names }}"
          when: group.admin | bool
          loop_control:
            loop_var: group
