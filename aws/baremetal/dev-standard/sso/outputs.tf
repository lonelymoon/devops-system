output "host" {
  value = module.keycloak_instance.app_host
}

output "url" {
  value = module.keycloak_instance.app_url
}

output "realm" {
  value = var.app_preconfiguration.realms.sso.name
}

output "realm_url" {
  value = "${module.keycloak_instance.app_url}/auth/realms/${var.app_preconfiguration.realms.sso.name}"
}

output "realm_username" {
  value     = var.app_preconfiguration.realms.sso.username
  sensitive = true
}

output "realm_password" {
  value     = var.app_preconfiguration.realms.sso.password
  sensitive = true
}

output "master_realm" {
	value = var.app_preconfiguration.realms.master
}

output "master_realm_url" {
	value = "${module.keycloak_instance.app_url}/auth/realms/${var.app_preconfiguration.realms.master.name}"
}

output "master_realm_username" {
	value     = var.app_preconfiguration.realms.master.username
	sensitive = true
}

output "master_realm_password" {
	value     = var.app_preconfiguration.realms.master.password
	sensitive = true
}

output "realm_openid_well_known_url" {
	value = "${module.keycloak_instance.app_url}/auth/realms/${var.app_preconfiguration.realms.sso.name}/.well-known"
}

output "realm_openid_endpoint_url" {
  value = "${module.keycloak_instance.app_url}/auth/realms/${var.app_preconfiguration.realms.sso.name}/.well-known/openid-configuration"
}

output "realm_saml_metadata_url" {
  value = "${module.keycloak_instance.app_url}/auth/realms/${var.app_preconfiguration.realms.sso.name}/protocol/saml/descriptor"
}
