---
- hosts: all
  gather_facts: yes
  become: yes
  vars:
    jdbc:
      postgres:
        url: https://jdbc.postgresql.org/download/postgresql-42.2.8.jar
        name: postgresql
        module: org.postgresql
        package: org/postgresql/main
        data_source_class: org.postgresql.xa.PGXADataSource
      h2:
        name: h2
        module: com.h2database.h2
        data_source_class: org.h2.jdbcx.JdbcDataSource
  tasks:
    - name: Ansible Setup
      include_tasks: "{{ playbook_dir }}/subtasks/ansible_dependencies.yaml"

    - name: Pre-installation
      block:
        - name: Pre-installation - Create group ({{ install_group }})
          group:
            name: "{{ install_group }}"
            system: yes
            state: present

        - name: Pre-installation - Create user ({{ install_user }})
          user:
            name: "{{ install_user }}"
            system: yes
            groups:
              - ec2-user
              - adm
              - wheel
              - systemd-journal
              - "{{ install_group }}"
            append: yes

        - name: Pre-installation - Install JDK 8
          package:
            name: java-1.8.0-openjdk-devel
            state: latest

        - name: Pre-installation - Ensure directories exist
          file:
            path: "{{ item }}"
            group: "{{ install_group }}"
            owner: "{{ install_user }}"
            state: directory
          with_items:
            - "{{ install_dir }}"
            - "{{ install_tmp }}"

    - name: Installation
      block:
        - name: Installation - Download Keycloak distribution and unpack
          unarchive:
            src: "{{ install_url }}"
            dest: "{{ install_tmp }}"
            remote_src: yes

        - name: Installation - Install Keycloak
          copy:
            src: "{{ install_tmp }}/keycloak-{{ app_version }}/"
            dest: "{{ install_dir }}/"
            group: "{{ install_group }}"
            owner: "{{ install_user }}"
            remote_src: yes

    - name: Configuration
      vars:
        jdbc_driver_install_dir: "{{ install_dir }}/modules/system/layers/keycloak/{{ jdbc[db_engine]['package'] }}"
        keycloak_configuration_file: "{{ install_dir }}/standalone/configuration/standalone.xml"
      block:
        - name: Configuration - Database
          block:
            - name: Configuration - Database - Ensure JDBC package directory exists
              file:
                path: "{{ jdbc_driver_install_dir }}"
                group: "{{ install_group }}"
                owner: "{{ install_user }}"
                state: directory

            - name: Configuration - Database - Download JDBC Driver ({{ db_engine }})
              get_url:
                url: "{{ jdbc[db_engine]['url'] }}"
                dest: "{{ jdbc_driver_install_dir }}"
              register: jdbc_download

            - name: Configuration - Database - Generate JDBC Driver module.xml
              vars:
                jdbc_module_name: "{{ jdbc[db_engine]['module'] }}"
                jdbc_jar_name: "{{ jdbc_download.dest | basename }}"
              template:
                src: "{{ playbook_dir }}/templates/jdbc/module.xml"
                dest: "{{ jdbc_driver_install_dir }}/module.xml"
                group: "{{ install_group }}"
                owner: "{{ install_user }}"

            - name: Configuration - Database - Generate JDBC Driver configuration
              xml:
                path: "{{ keycloak_configuration_file }}"
                namespaces:
                  ns: urn:jboss:domain:datasources:5.0
                xpath: //ns:subsystem/ns:datasources/ns:drivers
                pretty_print: yes
                set_children:
                  - driver:
                      name: "{{ jdbc[db_engine]['name'] }}"
                      module: "{{ jdbc[db_engine]['module'] }}"
                      _:
                        - xa-datasource-class: "{{ jdbc[db_engine]['data_source_class'] }}"

                  - driver:
                      name: "{{ jdbc['h2']['name'] }}"
                      module: "{{ jdbc['h2']['module'] }}"
                      _:
                        - xa-datasource-class: "{{ jdbc['h2']['data_source_class'] }}"

            - name: Configuration - Database - Generate KeycloakDS datasource configuration
              xml:
                path: "{{ keycloak_configuration_file }}"
                namespaces:
                  ns: urn:jboss:domain:datasources:5.0
                xpath: //ns:subsystem/ns:datasources/ns:datasource[@pool-name='KeycloakDS']
                pretty_print: yes
                set_children:
                  - connection-url: jdbc:{{ jdbc[db_engine]['name'] }}://{{ db_host }}:{{ db_port }}/{{ db_name }}
                  - driver: "{{ jdbc[db_engine]['name'] }}"
                  - pool:
                      _:
                        - max-pool-size: "20"
                  - security:
                      _:
                        - user-name: "{{ db_username }}"
                        - password: "{{ db_password }}"
                state: present

        - name: Configuration - Reverse Proxy
          block:
            - name: Configuration - Reverse Proxy - Generate http proxy forwarding configuration
              xml:
                path: "{{ keycloak_configuration_file }}"
                namespaces:
                  ns: urn:jboss:domain:undertow:9.0
                xpath: //ns:subsystem/ns:server/ns:http-listener
                pretty_print: yes
                attribute: proxy-address-forwarding
                value: "true"
                state: present

            - name: Configuration - Reverse Proxy - Generate https proxy forwarding configuration
              xml:
                path: "{{ keycloak_configuration_file }}"
                namespaces:
                  ns: urn:jboss:domain:undertow:9.0
                xpath: //ns:subsystem/ns:server/ns:https-listener
                pretty_print: yes
                attribute: proxy-address-forwarding
                value: "true"
                state: present

        - name: Configuration - System Service
          vars:
            service_name: keycloak
          block:
            - name: Configuration - System Service - Generate systemd service configuration
              template:
                src: "{{ playbook_dir }}/templates/{{ service_name }}.service"
                dest: /etc/systemd/system/{{ service_name }}.service
                group: "{{ install_group }}"
                owner: "{{ install_user }}"
                mode: "0755"

            - name: Configuration - System Service - Start & Enable systemd service
              service:
                name: "{{ service_name }}"
                enabled: yes
                state: started
