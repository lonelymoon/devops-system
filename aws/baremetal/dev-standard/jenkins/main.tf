module "master_instance" {
	source = "../../../../common/terraform/modules/aws/app_instance"

	app_name     = var.app_name
	app_version  = var.app_version
	app_web_port = var.app_web_port

	additional_security_groups = []

	root_block_device_type = "gp2"
	root_block_device_size = 8

	keypair                = var.aws.keypair
	vpc_id                 = var.vpc.vpc_id
	subnets                = var.vpc.private_subnets
	bastion_security_group = var.bastion.security_group
	alb_arn                = var.alb.alb.arn
	alb_security_group     = var.alb.security_group
	storage_bucket         = var.storage.s3.id
	dns_domain_name        = var.dns.domain_name
	dns_public_zone_id     = var.dns.public_zone.zone_id
	dns_private_zone_id    = var.dns.private_zone.zone_id

	tags = merge(var.tags,
	{
		Component : "master"
	})
}

resource "null_resource" "jenkins_installation" {
	triggers = {
		installation = filemd5("${path.module}/ansible/master/install.yaml")
	}

	depends_on = [
		module.master_instance,
	]

	connection {
		type                = "ssh"
		host                = module.master_instance.app_instance_private_ip
		port                = var.app_ssh_port
		user                = var.app_ssh_username
		private_key         = var.bastion.private_key
		bastion_host        = var.bastion.public_host
		bastion_port        = var.bastion.port
		bastion_user        = var.bastion.username
		bastion_private_key = var.bastion.private_key
	}

	provisioner "ansible" {
		defaults {
			become_method = "sudo"
			become_user   = "root"
		}

		plays {
			hosts   = [module.master_instance.app_instance_private_ip]
			enabled = true
			verbose = true

			playbook {
				file_path = "${path.module}/ansible/master/install.yaml"
				tags      = []
			}

			extra_vars = {
				app_name         = var.app_name
				app_version      = var.app_version
				app_url          = module.master_instance.app_url
				app_host         = module.master_instance.app_host
				app_web_port     = var.app_web_port
				install_url      = local.app_installation.url
				install_group    = local.app_installation.group
				install_user     = local.app_installation.user
				install_home_dir = local.app_installation.home_dir
				install_dir      = local.app_installation.dir
				install_tmp      = local.app_installation.tmp

				plugins_json = jsonencode(var.app_preconfiguration.plugins)
			}
		}
	}
}

resource "null_resource" "jenkins_preconfiguration" {
	triggers = {
		configuration = filemd5("${path.module}/ansible/master/configure.yaml")
	}

	depends_on = [
		module.master_instance,
		null_resource.jenkins_installation,
	]

	connection {
		type                = "ssh"
		host                = module.master_instance.app_instance_private_ip
		port                = var.app_ssh_port
		user                = var.app_ssh_username
		private_key         = var.bastion.private_key
		bastion_host        = var.bastion.public_host
		bastion_port        = var.bastion.port
		bastion_user        = var.bastion.username
		bastion_private_key = var.bastion.private_key
	}

	provisioner "ansible" {
		defaults {
			become_method = "sudo"
			become_user   = "root"
		}

		plays {
			hosts   = [module.master_instance.app_instance_private_ip]
			enabled = true
			verbose = true

			playbook {
				file_path = "${path.module}/ansible/master/configure.yaml"
				tags      = []
			}

			extra_vars = {
				app_name         = var.app_name
				app_version      = var.app_version
				app_url          = module.master_instance.app_url
				app_host         = module.master_instance.app_host
				app_web_port     = var.app_web_port
				install_url      = local.app_installation.url
				install_group    = local.app_installation.group
				install_user     = local.app_installation.user
				install_home_dir = local.app_installation.home_dir
				install_dir      = local.app_installation.dir
				install_tmp      = local.app_installation.tmp

				sso_enabled                   = var.app_preconfiguration.sso.enabled
				sso_url                       = var.sso_url
				sso_realm                     = var.sso_realm
				sso_realm_url                 = var.sso_realm_url
				sso_realm_username            = var.sso_realm_username
				sso_realm_password            = var.sso_realm_password
				sso_realm_openid_endpoint_url = var.sso_realm_openid_endpoint_url
				sso_client_name               = var.app_name

				plugins_json = jsonencode(var.app_preconfiguration.plugins)
			}
		}
	}
}
