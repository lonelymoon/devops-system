output "vpc" {
  value = module.vpc.vpc
}

output "dns" {
  value = module.dns
}

output "storage" {
  value = module.storage
}

output "alb" {
  value = module.alb
}

output "bastion" {
  value     = module.bastion
  sensitive = true
}
