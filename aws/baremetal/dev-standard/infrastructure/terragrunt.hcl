include {
  path = find_in_parent_folders()
}

locals {
  devops_system_config = yamldecode(file(find_in_parent_folders("devops_system_config.yaml")))
  env_config           = yamldecode(file(find_in_parent_folders("env_config.yaml")))
}

inputs = {
  vpc_name             = local.devops_system_config.infrastructure.vpc.name
  vpc_cidr             = local.devops_system_config.infrastructure.vpc.cidr
  domain_name          = local.devops_system_config.infrastructure.dns.domain
  domain_name_internal = local.devops_system_config.infrastructure.dns.internal
  bastion_ssh_port     = local.devops_system_config.infrastructure.bastion.ssh_port
  bastion_ssh_key      = local.devops_system_config.infrastructure.bastion.ssh_key

  tags = merge(
    local.env_config.aws.common_tags,
    {
      Module : "infrastructure",
      Environment : local.env_config.terraform.environment,
    }
  )
}
