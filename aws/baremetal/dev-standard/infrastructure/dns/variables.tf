variable "domain_name" {
  type = string
}

variable "domain_name_internal" {
  type = string
}

variable "vpc_id" {
  type = string
}

variable "tags" {
  type = map
}
