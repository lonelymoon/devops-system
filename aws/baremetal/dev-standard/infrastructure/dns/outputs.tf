output "domain_name" {
  value = var.domain_name
}

output "public_zone" {
  value = data.aws_route53_zone.public
}

output "private_zone" {
  value = aws_route53_zone.private
}
