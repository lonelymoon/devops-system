data "aws_route53_zone" "public" {
  name         = "${var.domain_name}."
  private_zone = false
}

resource "aws_route53_zone" "private" {
  name          = var.domain_name
  # name          = var.domain_name_internal
  force_destroy = true

  vpc {
    vpc_id = var.vpc_id
  }

  tags = var.tags
}

# resource "aws_vpc_dhcp_options" "internal" {
#   domain_name         = var.domain_name_internal
#   domain_name_servers = ["AmazonProvidedDNS"]

#   tags = var.tags
# }

# resource "aws_vpc_dhcp_options_association" "dns_resolver" {
#   vpc_id          = var.vpc_id
#   dhcp_options_id = aws_vpc_dhcp_options.internal.id
# }
