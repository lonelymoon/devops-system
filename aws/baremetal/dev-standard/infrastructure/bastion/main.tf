resource "aws_autoscaling_group" "bastion" {
  name               = "bastion"
  availability_zones = data.aws_availability_zones.available.names

  launch_template {
    name    = aws_launch_template.bastion.name
    version = "$Latest"
  }

  max_size         = 1
  min_size         = 0
  desired_capacity = 1

  health_check_type         = "EC2"
  health_check_grace_period = 300

  tag {
    key                 = "Name"
    value               = "bastion"
    propagate_at_launch = true
  }

  dynamic "tag" {
    for_each = var.tags

    content {
      key                 = tag.key
      value               = tag.value
      propagate_at_launch = true
    }
  }
}

data "aws_availability_zones" "available" {
  state = "available"
}

resource "aws_launch_template" "bastion" {
  name          = "bastion"
  instance_type = var.instance_type
  image_id      = data.aws_ami.amz_linux_2.image_id

  key_name = var.key_pair

  user_data = data.template_cloudinit_config.bastion_cloud_config.rendered

  network_interfaces {
    network_interface_id = aws_network_interface.bastion.id
  }

  tag_specifications {
    resource_type = "instance"
    tags          = var.tags
  }

  tag_specifications {
    resource_type = "volume"
    tags          = var.tags
  }

  tags = var.tags
}

data "aws_ami" "amz_linux_2" {
  owners      = ["amazon"]
  most_recent = true

  filter {
    name   = "name"
    values = ["amzn2-ami-hvm-2.0.????????.?-x86_64-gp2"]
  }

  filter {
    name   = "state"
    values = ["available"]
  }
}

data "template_cloudinit_config" "bastion_cloud_config" {
  gzip          = true
  base64_encode = true

  part {
    filename     = "init.cfg"
    content_type = "text/cloud-config"
    content      = "${data.template_file.bastion_user_data.rendered}"
  }
}

data "template_file" "bastion_user_data" {
  template = file("${path.module}/user_data/cloud_config.yaml")

  vars = {
    ssh_port = var.ssh_port
  }
}

resource "aws_eip" "bastion" {
  vpc               = true
  network_interface = aws_network_interface.bastion.id

  tags = merge(
    var.tags,
    {
      Name : "bastion"
    }
  )
}

resource "aws_network_interface" "bastion" {
  subnet_id = var.subnets[0]

  security_groups = [aws_security_group.bastion_instance.id]
}

resource "aws_route53_record" "bastion_public" {
  zone_id = var.dns_public_zone_id
  name    = "bastion.${var.domain_name}"
  type    = "A"
  ttl     = 300
  records = [aws_eip.bastion.public_ip]
}

resource "aws_route53_record" "bastion_private" {
  zone_id = var.dns_private_zone_id
  # name    = "bastion.${var.domain_name}"
	name 		= "bastion"
  type    = "A"
  ttl     = 300
  records = [aws_eip.bastion.private_ip]
}

resource "aws_security_group" "bastion_instance" {
  name   = "bastion_instance"
  vpc_id = data.aws_vpc.devops-systems.id

  ingress {
    from_port   = var.ssh_port
    to_port     = var.ssh_port
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = var.tags
}

data "aws_vpc" "devops-systems" {
  id = var.vpc_id
}
