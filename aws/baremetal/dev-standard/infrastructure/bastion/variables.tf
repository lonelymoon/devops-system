variable "ssh_port" {
  type    = number
  default = 22
}

variable "ssh_key" {
  type = string
}

variable "subnets" {
  type = list(string)
}

variable "instance_type" {
  type    = string
  default = "t2.micro"
}

variable "vpc_id" {
  type = string
}

variable "key_pair" {
  type = string
}

variable "domain_name" {
  type = string
}

variable "dns_public_zone_id" {
  type = string
}

variable "dns_private_zone_id" {
  type = string
}

variable "tags" {
  type = map
}
