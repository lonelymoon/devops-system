output "public_host" {
  value = aws_route53_record.bastion_public.name
}

output "public_ip" {
  value = aws_eip.bastion.public_ip
}

output "private_host" {
  value = aws_route53_record.bastion_private.name
}

output "private_ip" {
  value = aws_eip.bastion.private_ip
}

output "port" {
  value = var.ssh_port
}

output "username" {
  value = "ec2-user"
}

output "private_key" {
  value     = file(var.ssh_key)
  sensitive = true
}

output "security_group" {
  value = aws_security_group.bastion_instance.id
}
