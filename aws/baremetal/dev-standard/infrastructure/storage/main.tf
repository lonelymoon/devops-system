data "aws_caller_identity" "current" {}

resource "aws_s3_bucket" "devops_systems" {
  bucket = var.bucket_name
  acl    = "private"

  versioning {
    enabled = true
  }

  tags = var.tags
}

resource "aws_s3_bucket_public_access_block" "devops_systems" {
  bucket = aws_s3_bucket.devops_systems.id

  block_public_acls       = true
  block_public_policy     = true
  ignore_public_acls      = true
  restrict_public_buckets = true
}

# resource "aws_s3_bucket_policy" "devops_systems" {
#   bucket = aws_s3_bucket.devops_systems.id

#   policy = data.aws_iam_policy_document.devops_systems_bucket_policy.json
# }

data "aws_iam_policy_document" "devops_systems_bucket_policy" {
  policy_id = "devops_systems_bucket_policy"

  statement {
    sid = "AllowUpload"

    principals {
      type        = "AWS"
      identifiers = [data.aws_caller_identity.current.arn]
    }

    resources = [
      "arn:aws:s3:::${var.bucket_name}/*",
    ]

    actions = [
      "s3:PutObject",
    ]
  }

  statement {
    sid = "VPCEndpointAccessOnly"

    effect = "Deny"

    principals {
      type        = "AWS"
      identifiers = ["*"]
    }

    resources = [
      "arn:aws:s3:::${var.bucket_name}/*",
    ]

    actions = [
      "s3:GetObject",
    ]

    condition {
      test     = "StringNotEquals"
      variable = "aws:sourceVpce"

      values = [
        var.s3_vpc_endpoint_id
      ]
    }
  }
}
