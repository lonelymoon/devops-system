variable "bucket_name" {
  type    = string
  default = "akawork-devops-systems"
}

variable "s3_vpc_endpoint_id" {
  type = string
}

variable "tags" {
  type = map
}
