variable "aws" {
	type = object({
		region : string,
		keypair : string,
		common_tags : map(string),
	})
}

variable "vpc_name" {
	type = string
}

variable "vpc_cidr" {
	type = string
}

variable "domain_name" {
	type = string
}

variable "domain_name_internal" {
	type = string
}

variable "bastion_ssh_port" {
	type = string
}

variable "bastion_ssh_key" {
	type = string
}

variable "tags" {
	type = map(string)
}


