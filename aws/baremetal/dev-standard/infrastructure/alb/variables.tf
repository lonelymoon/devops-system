variable "vpc_id" {
  type = string
}

variable "domain_name" {
  type = string
}

variable "alb_name" {
  type    = string
  default = "devops-systems-alb"
}

variable "alb_subnets" {
  type = list
}

variable "tags" {
  type = map
}
