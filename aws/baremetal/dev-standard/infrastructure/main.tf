module "vpc" {
  source = "./vpc"

  vpc_name = var.vpc_name
  vpc_cidr = var.vpc_cidr

  tags = merge(
    var.tags,
    {
      Component : "vpc"
    }
  )
}

module "dns" {
  source = "./dns"

  vpc_id               = module.vpc.vpc.vpc_id
  domain_name          = var.domain_name
  domain_name_internal = var.domain_name_internal

  tags = merge(
    var.tags,
    {
      Component : "dns"
    }
  )
}

module "alb" {
  source = "./alb"

  vpc_id      = module.vpc.vpc.vpc_id
  domain_name = var.domain_name
  alb_subnets = module.vpc.vpc.public_subnets

  tags = merge(
    var.tags,
    {
      Component : "application-load-balancer"
    }
  )
}

module "storage" {
  source = "./storage"

  s3_vpc_endpoint_id = module.vpc.vpc_endpoints.endpoints.s3.id

  tags = merge(
    var.tags,
    {
      Component : "storage"
    }
  )
}

module "bastion" {
  source = "./bastion"

  ssh_port = var.bastion_ssh_port
  ssh_key  = var.bastion_ssh_key
  key_pair = var.aws.keypair

  vpc_id  = module.vpc.vpc.vpc_id
  subnets = module.vpc.vpc.public_subnets

  domain_name         = var.domain_name
  dns_public_zone_id  = module.dns.public_zone.zone_id
  dns_private_zone_id = module.dns.private_zone.zone_id

  tags = merge(
    var.tags,
    {
      Component : "bastion"
    }
  )
}
