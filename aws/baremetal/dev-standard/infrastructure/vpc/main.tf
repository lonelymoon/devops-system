data "aws_availability_zones" "available" {
  state = "available"
}

module "vpc" {
  source  = "terraform-aws-modules/vpc/aws"
  version = "~> 3.2"

  name = var.vpc_name
  cidr = var.vpc_cidr

  azs = data.aws_availability_zones.available.names
  public_subnets = [
    for num in range(length(data.aws_availability_zones.available.names) * 0 + 1, length(data.aws_availability_zones.available.names) * 1 + 1) :
    cidrsubnet(var.vpc_cidr, 8, num)
  ]
  private_subnets = [
    for num in range(length(data.aws_availability_zones.available.names) * 1 + 1, length(data.aws_availability_zones.available.names) * 2 + 1) :
    cidrsubnet(var.vpc_cidr, 8, num)
  ]
  intra_subnets = [
    for num in range(length(data.aws_availability_zones.available.names) * 2 + 1, length(data.aws_availability_zones.available.names) * 3 + 1) :
    cidrsubnet(var.vpc_cidr, 8, num)
  ]

  # enable_logs_endpoint = false
  # logs_endpoint_security_group_ids  = [aws_security_group.vpce_interface.id]
  # logs_endpoint_private_dns_enabled = true

  # enable_monitoring_endpoint = false
  # monitoring_endpoint_security_group_ids  = [aws_security_group.vpce_interface.id]
  # monitoring_endpoint_private_dns_enabled = true

  enable_nat_gateway     = true
  single_nat_gateway     = true
  one_nat_gateway_per_az = false
  enable_vpn_gateway     = false

  default_security_group_name = var.vpc_name
  default_security_group_tags = var.tags

  enable_dns_hostnames = true
  enable_dns_support   = true

  tags = var.tags
}

module "vpc_endpoints" {
  source = "terraform-aws-modules/vpc/aws//modules/vpc-endpoints"
  vpc_id = module.vpc.vpc_id
  security_group_ids = [
    aws_security_group.vpce_interface.id
  ]

  endpoints = {
    s3 = {
      service = "s3"
      tags    = var.tags
    },
  }

  tags = var.tags
}

resource "aws_security_group" "vpce_interface" {
  name   = "devops-systems-vpce-endpoint"
  vpc_id = module.vpc.vpc_id

  ingress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = [module.vpc.vpc_cidr_block]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = var.tags
}
