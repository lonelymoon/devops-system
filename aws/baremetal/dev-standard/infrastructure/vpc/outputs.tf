output "vpc" {
  value = module.vpc
}

output "vpc_endpoints" {
  value = module.vpc_endpoints
}
