module "gitlab_instance" {
	source = "../../../../common/terraform/modules/aws/app_instance"

	app_name     = var.app_name
	app_version  = var.app_version
	app_web_port = var.app_web_port

	additional_security_groups = []

	root_block_device_type = "gp2"
	root_block_device_size = 8

	keypair                = var.aws.keypair
	vpc_id                 = var.vpc.vpc_id
	subnets                = var.vpc.private_subnets
	bastion_security_group = var.bastion.security_group
	alb_arn                = var.alb.alb.arn
	alb_security_group     = var.alb.security_group
	storage_bucket         = var.storage.s3.id
	dns_domain_name        = var.dns.domain_name
	dns_public_zone_id     = var.dns.public_zone.zone_id
	dns_private_zone_id    = var.dns.private_zone.zone_id

	tags = merge(var.tags,
	{
		Component : "app"
	})
}

resource "null_resource" "gitlab_installation" {
	depends_on = [
		module.gitlab_instance,
		module.gitlab_db,
	]

	triggers = {
		installation = filemd5("${path.module}/ansible/install.yaml")
	}

	connection {
		type                = "ssh"
		host                = module.gitlab_instance.app_instance_private_ip
		port                = var.app_ssh_port
		user                = var.app_ssh_username
		private_key         = var.bastion.private_key
		bastion_host        = var.bastion.public_host
		bastion_port        = var.bastion.port
		bastion_user        = var.bastion.username
		bastion_private_key = var.bastion.private_key
	}

	provisioner "ansible" {
		defaults {
			become_method = "sudo"
			become_user   = "root"
		}

		plays {
			hosts   = [module.gitlab_instance.app_instance_private_ip]
			enabled = true
			verbose = true

			playbook {
				file_path = "${path.module}/ansible/install.yaml"
				tags      = []
			}

			extra_vars = {
				app_version      = var.app_version
				app_edition      = var.app_edition
				app_url          = module.gitlab_instance.app_url
				install_url      = local.app_installation.url
				install_group    = local.app_installation.group
				install_user     = local.app_installation.user
				install_dir      = local.app_installation.dir
				install_tmp      = local.app_installation.tmp
				db_engine        = module.gitlab_db.engine
				db_host          = module.gitlab_db.host
				db_port          = module.gitlab_db.port
				db_name          = module.gitlab_db.name
				db_username      = module.gitlab_db.username
				db_password      = module.gitlab_db.password
				whitelist_cidr   = module.gitlab_instance.app_instance_vpc_cidr
				initial_username = var.app_preconfiguration.initial_username
				initial_password = var.app_preconfiguration.initial_password
			}
		}
	}
}

resource "null_resource" "gitlab_bastion_configuration" {
	depends_on = [
		null_resource.gitlab_installation
	]

	triggers = {
		installation          = module.gitlab_instance.app_instance_id
		bastion_configuration = filemd5("${path.module}/ansible/bastion.yaml")
	}

	connection {
		type        = "ssh"
		host        = var.bastion.public_host
		port        = var.bastion.port
		user        = var.bastion.username
		private_key = var.bastion.private_key
	}

	provisioner "ansible" {
		defaults {
			become_method = "sudo"
			become_user   = "root"
		}

		plays {
			hosts   = [var.bastion.public_host]
			enabled = true
			verbose = true

			playbook {
				file_path = "${path.module}/ansible/bastion.yaml"
				tags      = []
			}

			extra_vars = {
				app_instance_private_ip = module.gitlab_instance.app_instance_private_ip
				app_name                = var.app_name
			}
		}
	}
}

resource "null_resource" "gitlab_preconfiguration" {
	depends_on = [
		null_resource.gitlab_installation
	]

	triggers = {
		app_preconfiguration = jsonencode(var.app_preconfiguration)
		configuration        = filemd5("${path.module}/ansible/configure.yaml")
	}

	connection {
		type                = "ssh"
		host                = module.gitlab_instance.app_instance_private_ip
		port                = var.app_ssh_port
		user                = var.app_ssh_username
		private_key         = var.bastion.private_key
		bastion_host        = var.bastion.public_host
		bastion_port        = var.bastion.port
		bastion_user        = var.bastion.username
		bastion_private_key = var.bastion.private_key
	}

	provisioner "ansible" {
		defaults {
			become_method = "sudo"
			become_user   = "root"
		}

		plays {
			hosts   = [module.gitlab_instance.app_instance_private_ip]
			enabled = var.app_preconfiguration.enabled
			verbose = true

			playbook {
				file_path = "${path.module}/ansible/configure.yaml"
				tags      = []
			}

			extra_vars = {
				app_url            = module.gitlab_instance.app_url
				install_dir        = local.app_installation.dir
				install_tmp        = local.app_installation.tmp
				sso_url            = var.sso_url
				sso_realm          = var.sso_realm
				sso_realm_url      = var.sso_realm_url
				sso_realm_username = var.sso_realm_username
				sso_realm_password = var.sso_realm_password

				sso_realm_saml_metadata_url = var.sso_realm_saml_metadata_url
				sso_client_name             = var.app_name
				groups_json                 = jsonencode(var.app_seed_data.groups)
			}
		}
	}
}

module "gitlab_db" {
	source = "../../../../common/terraform/modules/aws/app_db"

	app_name             = var.app_name
	engine               = var.db_engine
	engine_version       = var.db_engine_version
	major_engine_version = var.db_major_engine_version
	family               = var.db_family
	instance_class       = var.db_instance_class
	multi_az             = var.db_multi_az
	storage_size         = var.db_storage_size
	storage_type         = var.db_storage_type
	name                 = var.db_name
	port                 = var.db_port
	username             = var.db_username
	password             = var.db_password

	vpc_id  = var.vpc.vpc_id
	subnets = var.vpc.intra_subnets

	tags = merge(var.tags,
	{
		Component : "db",
	})
}
