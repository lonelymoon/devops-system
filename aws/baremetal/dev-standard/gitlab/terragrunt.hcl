skip = local.devops_system_config[local.app_name].disabled

include {
  path = find_in_parent_folders()
}

dependency "infrastructure" {
  config_path = "../infrastructure"
}

dependency "sso" {
  config_path = "../sso"
}

locals {
  devops_system_config    = yamldecode(file(find_in_parent_folders("devops_system_config.yaml")))
  devops_system_seed_data = yamldecode(file(find_in_parent_folders("devops_system_seed_data.yaml")))
  env_config              = yamldecode(file(find_in_parent_folders("env_config.yaml")))
  app_name                = "gitlab"
}

inputs = {
  vpc     = dependency.infrastructure.outputs.vpc
  alb     = dependency.infrastructure.outputs.alb
  dns     = dependency.infrastructure.outputs.dns
  storage = dependency.infrastructure.outputs.storage
  bastion = dependency.infrastructure.outputs.bastion

  app_name             = local.app_name
  app_edition          = local.devops_system_config[local.app_name].app.edition
  app_version          = local.devops_system_config[local.app_name].app.version
  app_preconfiguration = local.devops_system_config[local.app_name].app.preconfiguration
  app_web_port         = local.devops_system_config[local.app_name].app.ports["web"]
  app_seed_data        = local.devops_system_seed_data

  sso_url                     = dependency.sso.outputs.url
  sso_realm                   = dependency.sso.outputs.realm
  sso_realm_url               = dependency.sso.outputs.realm_url
  sso_realm_username          = dependency.sso.outputs.realm_username
  sso_realm_password          = dependency.sso.outputs.realm_password
  sso_realm_saml_metadata_url = dependency.sso.outputs.realm_saml_metadata_url

  db_engine               = local.devops_system_config[local.app_name].db.engine
  db_engine_version       = local.devops_system_config[local.app_name].db.engine_version
  db_major_engine_version = local.devops_system_config[local.app_name].db.major_engine_version
  db_family               = local.devops_system_config[local.app_name].db.family
  db_instance_class       = local.devops_system_config[local.app_name].db.instance_class
  db_storage_size         = local.devops_system_config[local.app_name].db.storage_size
  db_storage_type         = local.devops_system_config[local.app_name].db.storage_type
  db_name                 = local.devops_system_config[local.app_name].db.name
  db_port                 = local.devops_system_config[local.app_name].db.port
  db_username             = local.devops_system_config[local.app_name].db.username
  db_password             = local.devops_system_config[local.app_name].db.password

  tags = merge(local.env_config.aws.common_tags,
  {
    Module : local.app_name,
    Environment : local.env_config.terraform.environment,
  })
}
