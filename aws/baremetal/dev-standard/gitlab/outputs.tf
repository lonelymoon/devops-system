output "host" {
  value = module.gitlab_instance.app_host
}

output "url" {
  value = module.gitlab_instance.app_url
}