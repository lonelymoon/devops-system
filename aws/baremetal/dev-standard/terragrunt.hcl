locals {
  devops_system_config = yamldecode(file("devops_system_config.yaml"))
  env_config           = yamldecode(file("env_config.yaml"))
}

remote_state {
  backend = "s3"

  generate = {
    path      = "backend.tf"
    if_exists = "overwrite_terragrunt"
  }

  config = {
    region              = local.env_config.aws.region
    bucket              = local.env_config.terraform.bucket
    key                 = "${local.env_config.terraform.environment}/${path_relative_to_include()}/terraform.tfstate"
    s3_bucket_tags      = tomap(local.env_config.aws.common_tags)
    encrypt             = true
    dynamodb_table      = local.env_config.terraform.lock_table
    dynamodb_table_tags = tomap(local.env_config.aws.common_tags)
  }
}

inputs = {
  aws                  = local.env_config.aws
  devops_system_config = local.devops_system_config
}

generate "provider" {
  path      = "provider.tf"
  if_exists = "overwrite_terragrunt"
  contents  = file("${get_parent_terragrunt_dir()}/provider.template.hcl")
}

terraform {
  # extra_arguments "debug_logging" {
  #   commands = [
  #     "apply"
  #   ]

  #   env_vars = {
  #     TF_LOG      = "DEBUG"
  #     TF_LOG_PATH = "${basename(get_terragrunt_dir())}.tf.log"
  #   }
  # }

  # extra_arguments "plan_out" {
  #   commands = [
  #     "plan",
  #   ]

  #   arguments = [
  #     "-out=${basename(get_terragrunt_dir())}.tfplan"
  #   ]
  # }

  # extra_arguments "apply_plan" {
  #   commands = [
  #     "apply",
  #   ]

  #   arguments = [
  #     "${get_terragrunt_dir()}/${basename(get_terragrunt_dir())}.tfplan"
  #   ]
  # }

  # after_hook "after_hook_apply" {
  #   commands = ["apply"]
  #   execute  = ["sh", "-c", "terraform show -json > state.json"]
  # }
}
