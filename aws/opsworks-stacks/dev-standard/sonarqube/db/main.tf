data "aws_availability_zones" "available" {
  state = "available"
}

resource "aws_security_group" "sonarqubedb" {
  name   = "sonarqubedb"
  vpc_id = var.vpc_id

  ingress {
    from_port = var.db_port
    to_port   = var.db_port
    protocol  = "tcp"
    cidr_blocks = [
      var.vpc_cidr
    ]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = var.tags
}

module "db" {
  source  = "terraform-aws-modules/rds/aws"
  version = "~> 2.0"

  identifier          = "sonarqubedb"
  engine              = var.engine
  engine_version      = var.engine_version
  instance_class      = var.instance_class
  allocated_storage   = var.storage_size
  storage_type        = var.storage_type
  storage_encrypted   = false
  publicly_accessible = false

  # DB parameter group
  family = "postgres10"

  # DB option group
  major_engine_version = "10"

  name     = var.db_name
  username = var.db_username
  password = var.db_password
  port     = var.db_port

  vpc_security_group_ids = [aws_security_group.sonarqubedb.id]
  subnet_ids             = var.subnet_ids

  maintenance_window              = "Thu:00:00-Thu:03:00"
  backup_window                   = "03:00-06:00"
  backup_retention_period         = 0
  enabled_cloudwatch_logs_exports = ["postgresql", "upgrade"]
  skip_final_snapshot             = true

  performance_insights_enabled = false

  tags = var.tags
}

resource "aws_opsworks_rds_db_instance" "sonarqube" {
  stack_id            = var.opsworks_stack_id
  rds_db_instance_arn = module.db.this_db_instance_arn
  db_user             = var.db_username
  db_password         = var.db_password
}
