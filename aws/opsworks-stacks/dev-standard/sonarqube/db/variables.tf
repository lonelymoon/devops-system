variable "tags" {
  type = map
}

variable "vpc_id" {
  type = string
}

variable "vpc_cidr" {
  type = string
}

variable "subnet_ids" {
  type = list
}

variable "opsworks_stack_id" {
  type = string
}

variable "engine" {
  default = "postgres"
}

variable "engine_version" {
  default = "10.10"
}

variable "instance_class" {
  default = "db.m4.large"
}

variable "storage_size" {
  default = 5
}

variable "storage_type" {
  default = "gp2"
}

variable "db_name" {
  default = "sonarqubedb"
}

variable "db_port" {
  default = "5432"
}

variable "db_username" {
  default = "sonarqubedbusr"
}

variable "db_password" {
  default = "sonarqubedbpwd"
}
