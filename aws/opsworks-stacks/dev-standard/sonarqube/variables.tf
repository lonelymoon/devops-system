variable "aws" {
  type = map
}

variable "vpc" {
  type = object({
    vpc_id : string,
    vpc_cidr_block : string,
    intra_subnets : list(string)
  })
}

variable "opsworks" {
  type = object({
    id : string,
  })
}

variable "db" {
  type = map
}

variable "common_tags" {
  type = map
}
