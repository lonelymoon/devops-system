module "db" {
  source = "./db"

  vpc_id     = var.vpc.vpc_id
  vpc_cidr   = var.vpc.vpc_cidr_block
  subnet_ids = var.vpc.intra_subnets

  opsworks_stack_id = var.opsworks.id

  tags = merge(
    var.common_tags,
    {
      Component : "db"
    }
  )
}

# module "app" {
#   source = "./app"
# }
