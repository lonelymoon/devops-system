include {
  path = find_in_parent_folders()
}

dependency "infrastructure" {
  config_path = "../infrastructure"

  mock_outputs = {
    vpc = {
      vpc_id   = "vpc-dummy-id"
      vpc_cidr = "vpc-dummy-cidr"
    }
  }

  mock_outputs_allowed_terraform_commands = ["validate"]
}

locals {
  common_vars = yamldecode(file("${get_terragrunt_dir()}/${find_in_parent_folders("common_vars.yaml")}"))

  common_tags = merge(
    local.common_vars.common_tags,
    {
      Module : "sonarqube",
      Environment : local.common_vars.terraform.environment,
    }
  )
}

inputs = {
  aws         = local.common_vars.aws
  common_tags = local.common_tags
  vpc         = dependency.infrastructure.outputs.networking.vpc
  opsworks    = dependency.infrastructure.outputs.opsworks.stack
}
