provider "aws" {
  version = "~> 2.23"
  region  = var.aws.region
}