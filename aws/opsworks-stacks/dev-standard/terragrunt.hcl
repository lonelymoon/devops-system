locals {
  common_vars = yamldecode(file("common_vars.yaml"))
  region      = local.common_vars.aws.region
  common_tags = local.common_vars.common_tags
  environment = local.common_vars.terraform.environment
}

remote_state {
  backend = "s3"

  config = {
    region              = local.region
    bucket              = "akawork-devops-system-tfstate"
    key                 = "${local.environment}/${path_relative_to_include()}/terraform.tfstate"
    s3_bucket_tags      = tomap(local.common_tags)
    encrypt             = true
    dynamodb_table      = "akawork-devops-system-tfstate-lock"
    dynamodb_table_tags = tomap(local.common_tags)
  }
}
