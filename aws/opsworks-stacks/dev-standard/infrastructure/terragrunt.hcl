include {
  path = find_in_parent_folders()
}

locals {
  common_vars = yamldecode(file("${get_terragrunt_dir()}/${find_in_parent_folders("common_vars.yaml")}"))

  common_tags = merge(
    local.common_vars.common_tags,
    {
      Module : "infrastructure",
      Component : "networking",
      Environment: local.common_vars.terraform.environment,
    }
  )
}

inputs = {
  aws = local.common_vars.aws
  vpc = {
    name = "devops-systems"
    cidr = "10.0.0.0/16"
  }
  common_tags = local.common_tags
}
