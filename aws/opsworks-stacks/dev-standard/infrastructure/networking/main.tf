data "aws_availability_zones" "available" {
  state = "available"
}

module "vpc" {
  source  = "terraform-aws-modules/vpc/aws"
  version = "2.17.0"

  name = var.vpc_name
  cidr = var.vpc_cidr

  azs = data.aws_availability_zones.available.names
  public_subnets = [
    for num in range(length(data.aws_availability_zones.available.names) * 0 + 1, length(data.aws_availability_zones.available.names) * 1 + 1) :
    cidrsubnet(var.vpc_cidr, 8, num)
  ]
  private_subnets = [
    for num in range(length(data.aws_availability_zones.available.names) * 1 + 1, length(data.aws_availability_zones.available.names) * 2 + 1) :
    cidrsubnet(var.vpc_cidr, 8, num)
  ]
  intra_subnets = [
    for num in range(length(data.aws_availability_zones.available.names) * 2 + 1, length(data.aws_availability_zones.available.names) * 3 + 1) :
    cidrsubnet(var.vpc_cidr, 8, num)
  ]

  enable_nat_gateway     = true
  single_nat_gateway     = true
  one_nat_gateway_per_az = false
  enable_vpn_gateway     = false

  enable_dns_hostnames = true
  enable_dns_support   = true

  tags = var.tags
}
