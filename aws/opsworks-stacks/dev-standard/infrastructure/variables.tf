variable "aws" {
  type = map
}

variable "vpc" {
  type = map
}

variable "common_tags" {
    type = map
}
