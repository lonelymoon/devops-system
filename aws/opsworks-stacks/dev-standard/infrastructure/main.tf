module "networking" {
  source = "./networking"

  vpc_name = var.vpc.name
  vpc_cidr = var.vpc.cidr

  tags = var.common_tags
}

module "opsworks" {
  source = "./opsworks"

  region               = var.aws.region
  vpc_id               = module.networking.vpc.vpc_id
  default_subnet_id    = module.networking.vpc.private_subnets[0]
  default_ssh_key_name = var.aws.keypair

  tags = var.common_tags
}
