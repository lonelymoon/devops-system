variable "stack_name" {
  type    = string
  default = "devops-systems-stack"
}

variable "region" {
  type = string
}

variable "vpc_id" {
  type = string
}

variable "default_subnet_id" {
  type = string
}

variable "configuration_manager_version" {
  type = string
  default = "12"
}

variable "default_os" {
  type = string
  default = "Amazon Linux 2018.03"
}

variable "default_root_device_type" {
  type = string
  default = "ebs"
}

variable "default_ssh_key_name" {
  type = string
}

variable "tags" {
  type = map
}
