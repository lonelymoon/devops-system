resource "aws_opsworks_stack" "main" {
  name                          = var.stack_name
  region                        = var.region
  vpc_id                        = var.vpc_id
  default_subnet_id             = var.default_subnet_id
  configuration_manager_version = var.configuration_manager_version
  color                         = "rgb(184, 133, 46)"

  service_role_arn             = module.iam_assumable_role_opswork.this_iam_role_arn
  default_instance_profile_arn = aws_iam_instance_profile.opswork.arn

  default_os               = var.default_os
  default_root_device_type = var.default_root_device_type
  default_ssh_key_name     = var.default_ssh_key_name

  tags = var.tags
}

module "iam_assumable_role_opswork" {
  source = "terraform-aws-modules/iam/aws//modules/iam-assumable-role"

  trusted_role_services = [
    "opsworks.amazonaws.com"
  ]

  create_role = true

  role_name         = "opswork"
  role_requires_mfa = false

  custom_role_policy_arns = [
    aws_iam_policy.opswork.arn
  ]
}

resource "aws_iam_policy" "opswork" {
  name   = "opswork"
  path   = "/"
  policy = "${data.aws_iam_policy_document.opswork.json}"
}

data "aws_iam_policy_document" "opswork" {
  statement {
    actions = [
      "ec2:*",
      "iam:PassRole",
      "cloudwatch:GetMetricStatistics",
      "cloudwatch:DescribeAlarms",
      "ecs:*",
      "elasticloadbalancing:*",
      "rds:*"
    ]
    resources = [
      "*"
    ]
  }
}

resource "aws_iam_instance_profile" "opswork" {
  name = "opswork"
  role = module.iam_assumable_role_opswork_instance.this_iam_role_name
}

module "iam_assumable_role_opswork_instance" {
  source = "terraform-aws-modules/iam/aws//modules/iam-assumable-role"

  trusted_role_services = [
    "ec2.amazonaws.com"
  ]

  create_role = true

  role_name         = "opswork_instance"
  role_requires_mfa = false

  custom_role_policy_arns = [
    aws_iam_policy.opswork.arn
  ]
}
